<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [1. Final Ingenieria 2 - Diciembre 2018](#1-final-ingenieria-2---diciembre-2018)
    - [1. La importancia del GCS](#1-la-importancia-del-gcs)
        - [Version resumida](#version-resumida)
    - [2. Métricas (MOI y GQM)](#2-métricas-moi-y-gqm)
        - [Version resumida](#version-resumida-1)
    - [3. Riesgos, mencionar 2 y describir un plan de contingencia o minimización](#3-riesgos-mencionar-2-y-describir-un-plan-de-contingencia-o-minimización)
        - [Version Resumida](#version-resumida)
    - [4. Diseño arquitectónico.(Cliente-servidor)](#4-diseño-arquitectónicocliente-servidor)
        - [Version resumida](#version-resumida-2)
    - [5. Tipos de mantenimiento.(adaptativo y perfectivo)](#5-tipos-de-mantenimientoadaptativo-y-perfectivo)
        - [Resumen](#resumen)
    - [6. Camino crítico](#6-camino-crítico)
- [2. Final Ingeniería 2 - Febrero 2da mesa 2019](#2-final-ingeniería-2---febrero-2da-mesa-2019)
    - [1. Definir encuestas y cuando se usan](#1-definir-encuestas-y-cuando-se-usan)
        - [Version resumida](#version-resumida-3)
    - [2. Qué es un hito](#2-qué-es-un-hito)
        - [Version resumida](#version-resumida-4)
    - [3. Definir Riesgo y Línea de corte](#3-definir-riesgo-y-línea-de-corte)
        - [Version Resumida](#version-resumida-1)
    - [4. Definir GCS y enumerar 3 elementos](#4-definir-gcs-y-enumerar-3-elementos)
        - [Version resumida](#version-resumida-5)
    - [5. Describir 2 principios de nielsen](#5-describir-2-principios-de-nielsen)
        - [Version resumida](#version-resumida-6)
    - [6. Definir Arquitectura en capas](#6-definir-arquitectura-en-capas)
        - [Version resumida](#version-resumida-7)
    - [7. Camino critico](#7-camino-critico)
- [3. Final Ingenieria 2 - Diciembre 2019](#3-final-ingenieria-2---diciembre-2019)
    - [1. Describa y defina linea base.](#1-describa-y-defina-linea-base)
        - [Version resumida](#version-resumida-8)
    - [2. Que es un hito](#2-que-es-un-hito)
        - [Version resumida](#version-resumida-9)
    - [3. Defina riesgo y describa linea de corte.](#3-defina-riesgo-y-describa-linea-de-corte)
        - [Version resumida](#version-resumida-10)
    - [4. Describa dos principios de Nielsen](#4-describa-dos-principios-de-nielsen)
        - [Version resumida](#version-resumida-11)
    - [5. Describa Punto Funcion](#5-describa-punto-funcion)
        - [Version resumida](#version-resumida-12)
    - [6. Definir cliente-servidor.](#6-definir-cliente-servidor)
        - [Version resumida](#version-resumida-13)
    - [7. Camino critico](#7-camino-critico-1)
- [4. Final ingenieria 2 - Primera fecha febrero 2020](#4-final-ingenieria-2---primera-fecha-febrero-2020)
    - [1. Gestión de configuración de software que es y describir el proceso](#1-gestión-de-configuración-de-software-que-es-y-describir-el-proceso)
        - [Version resumida](#version-resumida-14)
    - [2. Que es un riesgo y definir línea de corte](#2-que-es-un-riesgo-y-definir-línea-de-corte)
        - [Version resumida](#version-resumida-15)
    - [3. Que es juicio experto (técnica de estimación)](#3-que-es-juicio-experto-técnica-de-estimación)
        - [version resumida](#version-resumida)
    - [4. Describa 2 principios de Nielsen](#4-describa-2-principios-de-nielsen)
        - [Version resumida](#version-resumida-16)
    - [5. Describir arquitectura en capas](#5-describir-arquitectura-en-capas)
        - [version resumida](#version-resumida-1)
    - [4. Definir caja negra](#4-definir-caja-negra)
        - [Version resumida](#version-resumida-17)
    - [5. Hacer un camino crítico](#5-hacer-un-camino-crítico)
- [5. Final Ingenieria 2 - (Segunda) Febrero 2020](#5-final-ingenieria-2---segunda-febrero-2020)

<!-- markdown-toc end -->


# 1. Final Ingenieria 2 - Diciembre 2018
## 1. La importancia del GCS

**GCS** es una abreviatura que hace referencia a la Gestion de la
Configuracion del Software.

Es el proceso de identificar y definir los elementos del sistema, con
la finalidad de ir controlando el cambio a lo largo del ciclo de vida
del software, reportando y registrando el estado de dichos elementos y
las solicitudes de cambio; verificando que elementos esten completos y
sean los correctos en todo momento.

Dado que los cambios son inevitables a lo largo del ciclo de vida del
software, la GCS toma importancia en el contexto del proceso de
software dentro de una organizacion, ya que se trata de que sea
claro, cuales son los cambios, quien/es lo originaron, que elementos
cambiaron, y demas.

La GCS nos ayuda a responder ciertas preguntas acerca de como maneja
la organizacion el cambio:

* Como identifica y gestiona una organizacion las diferentes versiones
  de un programa (documentacion incluida) **de forma que estos cambios
  puedan ser aplicados eficientemente**.
  
* Quien **controla los cambios** antes y despues que el producto es
  entregado al cliente.
  
* Quien tiene la responsabilidad de **aprobar y dar prioridad a los
  cambios**.
  
* Como podemos garantizar que los cambios **se han llevado a cabo
  correctamente**.
  
* Que mecanismo se utiliza para **dar aviso de los nuevos cambios** a
  las partes interesadas.

### Version resumida
```
Dado que el cambio es inevitable a lo largo del ciclo de vida del
software, es escencial que sepamos manejarlo y tener control del
mismo. La GCS toma relevancia en el contexto del proceso del software
de una organizacion de forma que en cualquier momento sea posible
conocer la historia de cambios, e incluso que la inclusion de nuevos
cambios se haga de forma eficiente; es decir, tener total control
sobre como el software evoluciona.
```
## 2. Métricas (MOI y GQM)

Una metrica es una medida cuantitativa del grado en el que un software
o sistema cumple con cierto atributo.

Las metricas son importantes porque nos permiten:

* Entender que sucede durante el proceso de desarrollo y el
  mantenimiento.
  
* Mejorar nuestros procesos y nuestros productos.

* Controlar que es lo que sucede en nuestros proyectos.

* Evaluar la calidad de nuestros procesos y productos.

GQM, cuyo significado es Goal Question Metric, es un modelo que sirve
para crear metricas que midan cierto objetivo. Sirve para mejorar la
calidad de nuestro proyecto.

Consta de tres niveles:

1. **Nivel conceptual:** Donde se establece el objetivo(Goal) de la
   medicion.

2. **Nivel operativo:** Donde se definen un conjunto de preguntas con
   el fin de determinar si se cumple el objetivo.
   
3. **Nivel Cuantitativo:** Donde se definen un conjunto de metricas
   para cada pregunta del nivel operativo, de forma que podamos
   responder en forma cuantitativa a cada pregunta.

[nada que ver con metricas ja!]
MOI hace referencia las cualidades que debe tener un lider de
equipo. Ellas son:

* **M**otivacion al personal: Deben ser capaces de motivar a los
  profesionales a su cargo para mantener la produccion al maximo.
  
* **O**rganizacion: Debe saber modelar procesos para que el concepto
  inicial se transforme en el producto final.
  
* **I**ncentivacion de ideas e innovacion: Debe alentar al personal a
  crear y sentirse creativos.
  
### Version resumida
```
Una metrica es una medida cuantitativa del grado con el que un sistema
o programa cumple con cierto atributo.

GQM(Goal Question Metric) es un modelo que sirve para generar metricas
que sirven para medir cierto objetivo. Como tal sirve para agregar
calidad a nuestros proyectos. Se bassa en tres niveles: conceptual(se
plantea la meta/Goal), Operacional(se definen una serie de preguntas
para determinar si la meta se cumple) y cuantitativo(se asocian
metricas a cada pregunta para responderlas en forma cuantitativa)

MOI hace referencia a las caracteristicas que hacen a un buen lider de
equipo. Las mismas son **Motivacion al personal**, **Organizacion** e
**Incentivacion de ideas e innovacion**.
```
## 3. Riesgos, mencionar 2 y describir un plan de contingencia o minimización 

Un riesgo es un evento no deseado que tiene consecuencias
negativas. Como tal podemos decir que tiene 2 caracteristicas:

1. **Incertidumbre**, no es 100% seguro que ocurra.

2. **Perdida**. Su ocurrencia genera perdidas de algun tipo.

Los riesgos pueden ser de varios tipos:

* Riesgos de proyecto: Son aquellos que amenazan la planificacion(de cualquier tipo)

* Riesgo de proyecto: Son los que amenazan la calidad del producto.

* Riesgo de negocio: Son los que amenazan a la organizacion, ya sea la
  que lo desarrolla o la que lo distribuye.

Ademas los riesgos se pueden categorizar en:

* Genericos: Son los que pueden ocurrir en cualquier proyecto.

* Especificos: Son los que dependen de la naturaleza del proyecto.

**Riesgo1:**
Los servidores no soportan la cantidad de requests por
segundo.
**Plan de contingencia**
Se debera contratar otra instancia de servidor y aplicar un load
balancer.

**Riesgo2:**
El personal no esta suficientemente calificado en las tecnologias
**Plan de minimizacion***
Se destinaran 4 hs semanales a la extra capacitacion del personal.

**Riesgo3:**
Los requerimientos cambian constantemente.
**Plan de contingencia**
Documentar los requerimientos en base a los estandares 1362(ConOps) y
830(srs).

### Version Resumida
```
Un riesgo es un evento no deseado que genera consecuencias
negativas. Tiene 2 caracteristicas:

    * INCERTIDUMBRE: no existe la seguridad un 100% de que vaya a
    ocurrir.
    
    * PERDIDA: su ocurrencia genera perdida de algun tipo, ya sea en
    tiempo o dinero.

RIESGO 1

Cambio en la gerencia superior, decide cancelar el proyecto.

Crear la documentacion necesaria para dejar establecido la necesidad y
beneficios de construir el software.

RIESGO 2

Perdida de un lider de equipo.

Adoptar una organizacion de equipo centralizada democratica, de forma
que los lideres de equipo puedan rotar y todos adquirir experiencia
```


## 4. Diseño arquitectónico.(Cliente-servidor)

La arquitectura de software define la relacion entre los elementos
estructurales de un sistema para que el software cumpla con los
requerimientos.

Los sistemas se suelen dividir en subsistemas; el disenio
arquitectonico se basa en identificar estos subsistemas y establecer
un marco de comunicacion entre los mismos. Inciden en el cumplimiento
de los requerimientos, mas especificamente a los no funcionales como
rendimiento, seguridad, proteccion, mantenibilidad...

En cuanto a las diferentes arquitecturas, podemos agruparlas segun la
organizacion del sistema(repositorio, cliente/servidor, capas), segun
su manejo de control(centralizado-llamada/retorno y gestor; o
descentralizado-broadcast/eventos y gestor de interrupciones), segun su
descomposicion modular(en flujo de funciones y orientado a objetos) y
un ultimo gran grupo compuesto por los sistemas distribuidos con
arquitecturas como P2P,cliente/servidor y arquitectura orientada a
servicios.

En cuanto a la arquitectura **cliente/servidor** puede ser bien
distribuida o correr en forma local en una sola maquina.

En este tipo de arquitectura podemos distinguir tres elementos
estructurales:

* **Servidor** que tiene los datos y los ofrece mediante servicios.

* **Cliente** es quien solicita los datos al servidor.

* **red** es el medio por el cual fluyen las comunicaciones, el
  intermediario entre clientes y servidores.
  
(En el caso local estos tres componentes corren en la misma
computadora).

Una de las ventajas de esta arquitectura es que permite tener una
fuente de informacion que es entregada bajo demanda a quien la
solicite, es decir que no es necesario que el servidor conozca a cada
cliente, incluso los clientes no tienen que conocerse entre si... solo
deben conocer a los servidores.

Podemos dividir esta arquitectura en dos tipos:

* **Dos niveles**, donde tenemos cliente y servidor. El servidor es
  quien contiene la logica del sistema y los datos. En el caso del
  cliente puede ser de dos tipos, **ligero** donde se encarga solo de
  presentar datos y **pesado** donde ademas de presentar los datos
  contiene logica.
  
* **Multinivel**, donde tenemos los dos niveles anteriores, solo que
  se agrega otro nivel por detras del servidor sobre el cual se apoya
  el mismo. Por ejemplo... un nivel para la base de datos.

### Version resumida
```
La arquitectura de software se encarga de definir los elementos
estructurales de un sistema y su relacion para hacer posible que
cumpla los requisitos. Estos requisitos son mas que nada los no
funcionales, como mantenibilidad, seguridad, proteccion...

La arquitectura cliente/servidor puede ser distribuida o local y
definimos tres elementos:

* SERVIDOR: ofrece datos a traves de servicios.

* CLIENTE: solicita datos al servidor a traves de sus servicios.

* RED: el medio a traves del cual se da esta comunicacion.

Podemos dividir este tipo de arquitectura en 2 tipos:

* DOS NIVELES

Basicamente tenemos de un lado un servidor y del otro los clientes. En
el servidor se ejecuta logica del sistema y se administran los
datos. En el otro lado tenemos clientes que pueden ser LIVIANOS(solo
se encarga de la presentacion) o PESADOS(ademas de presentar los datos
contienen logica del sistema).

* MULTINIVEL

Donde la parte servidor de la arquitectura contiene por detras otros
niveles en los cuales se apoya para cumplir su funcion. Por ejemplo,
puede utilizar una base de datos.
```

## 5. Tipos de mantenimiento.(adaptativo y perfectivo)
El mantenimiento es la atencion que se hace al sistema a lo largo de
su evolucion despues que se ha entgregado.

Este mantenimiento se puede realizar a productos propios o
heredados. Se estima que el mantenimiento abarca del 40 al 70 por
ciento del costo total de desarrollo. Por esto es importante poder
definir cuando se ha cerrado el ciclo de mantenimiento del
software. Esto sucede cuando se vuelve mas costoso mantenerlo que
desarrollar un nuevo sistema.

Los tipos de mantenimiento son 4:

* **Mantenimiento preventivo:** aquel que se efectua antes de recibir
  una peticion, para facilitar el futuro mantenimiento. Se estima que
  representa el 4% del mantenimiento.
  
* **Mantenimiento perfectivo:** es el mantenimieto que se encarga de
  incorporar nuevas funcionalidades, de mejorarlo. Se estima que
  insume el 50% del mantenimiento.
  
* **Mantenimiento adaptativo:** es aquel que se encarga modificar el
  software para que interaccione correctamente con el entorno. Se
  estima que representa el 25% del mantenimiento total.
  
* **Mantenimiento correctivo:** Se encarga del diagnostico y
  correccion de errores. Se estima que representa el 21% del
  mantenimiento total.

### Resumen 
```
El manteniemiento se trata de seguir la evolucion del software una vez
que se ha entregado.

    MANTENIMIENTO ADAPTATIVO

Es el mantenimiento que se encarga de modificar el software para que
interactue correctamente con su entorno.

Se estima que representa un 25% del mantenimiento total.

    MANTENIMIENTO PERFECTIVO

Es el mantenimiento que se realiza para agregar funcionalidades y
mejoras al sistema. Se estima que representa un 50% del mantenimiento
total.
```
## 6. Camino crítico

(sin enunciado)
    
# 2. Final Ingeniería 2 - Febrero 2da mesa 2019
## 1. Definir encuestas y cuando se usan

Las encuestan son uno de los metodos de elicitacion de
requerimientos. Consisten en una serie de preguntas escritas que
sirven para obtener sensaciones generalizadas sobre diversos temas.

Se utilizan cuando se desea obtener informacion sobre un tema y se
desea hacer un sondeo en forma rapida, de caracter general y a bajo
costo. Tienen la ventaja de que se pueden realizar a un costo
relativamente bajo, a equipos que pueden estar dispersos
geograficamente.

### Version resumida
```
Las encuestas son una modalidad de elicitacion de requisitos
consistentes de un conjunto de preguntas en formato escrito con la
finalidad de aportar una vision general sobre cierto tema.

Se utilizan cuando se desea obtener una vision generalizada sobre
cierto tema en forma rapida y a bajo costo, a un grupo de personas que
pueden estar distribuidas geograficamente.
```

## 2. Qué es un hito

La planificacion temporal es el proceso mediante el cual se distribuye
el esfuerzo estimado a lo largo de la duracion del proyecto.

Es importante realizar una buena planificacion temporal, porque de ese
modo se logran clientes satisfechos, se reducen costos, se logra un
impacto en el mercado...

La planificacion temporal por lo tanto tiene como caracteristica tener
una fecha de fin. Como se determina esta fecha, es que nos define la
perspectiva de nuestra planificacion temporal: cuando la fecha la
decide el cliente y se debe respetar; o bien, cuando la fecha la
decide el equipo de desarrollo luego de un extenso analisis del
sistema.

Esta planificacion se basa en cinco principios:

* Compartimentacion: dividir el proyecto en tareas y actividades
  manejables.
  
* Interdependencia: identificar que tareas dependen de la
  finalizacion de otras tareas.
  
* Asignacion de tiempo.

* Responsabilidades definidas: Que cada tarea tenga un responsable
  bien definifo.
  
* Resultados definidos: Cada tarea debe tener un resultado
  establecido.
  
* Hitos definidos.

Los hitos son uno de los elementos de dicha planificacion, junto con
las tareas, entregas y tareas criticas. Basamos la planificacion
temporal en hitos, que no son mas que puntos en el tiempo que son
significativos ya sea porque se integra un modulo critico, o bien se
termina una funcionalidad.

Se define hito como:

"Algo" que se espera que este finalizado para cierta fecha, como por
ejemplo, un modulo testeado o caracteristica de funcionamiento; un
logro que sea facil de evaluar, testeable y notable.

### Version resumida
```
Un hito es "algo" que debe estar finalizado para cierta fecha, con la
caracteristica que debe ser testeable, validable y notable.

En otras palabras, define un estado del proyecto que es notable ya sea
porque agrega una funcionalidad critica... finaliza un modulo... en
los que nos basamos para realizar una planificacion temporal. De hecho
una de los principios de la planificacion temporal es tener hitos bien
definidos.

```
## 3. Definir Riesgo y Línea de corte

Riesgo se define como un evento indeseable cuyas consecuencias son
perjudiciales. Dicho de otra forma, son eventos que no son deseados y
en caso de ocurrir generan perdidas. De hecho podemos mencionar que
sus dos caracteristicas mas distintivas son:

* Incertidumbre: No existe un 100% de probabilidades de que ocurra.

* Perjudicial: En caso de que suceda va a haber perdidas, ya sea en
  dinero o tiempo, o ambas.
  
Los podemos calificar segun su campo de accion como:

* Riesgo de proyecto(aquellos que afectan a la planificacion)

* Riesgo de producto(aquellos que afectan al producto, como su
  rendimiento)
  
* Riesgo de negocio(aquellos que afectan a la organizacion que
  desarrolla o distribuye)
  
A la vez podemos calificar a los riesgos como: **genericos**(aquellos
que se presentan en todos los proyectos) y **Especificos**(aquellos
que son especificos del proyecto, su naturaleza y contexto).

Dado que los riesgos son indeseables y generan perdidas, es importante
saber manejarlos; ya sea minimizando la probabilidad de que ocurran o
minimizando la cantidad de danio que pueden causar.

Esto se realiza mediante la gestion de riesgos que consta de tareas
las cuales son:

* identificacion: se enumeran los riesgos y su tipo.

* analisis: se establece cual es su probabilidad e impacto. Con estas
  dos cantidades, se define una linea de corte que determinara que
  riesgos deben ser seguidos y cuales no.

* planeacion: Se establecen estrategias para **Evitar**, **minimizar
  consecuencias** o **tratar** el riesgo.

* supervision: Se realiza una retroalimentacion en donde se analiza
  que riesgos sucedieron, como fueron tratados, se modifican de ser
  necesario... e incluso pueden aparecer riesgos no contemplados
  previamente.
  
Definimos linea de corte como el limite entre los riesgos que seran
supervisados y cuales no. No hay un criterio unanime para definir en
que posicion se establece la linea de corte(bohem dice que 10.. otros
proponen la linea segun la cantidad de personas involucradas y la
complejidad del proyecto)

### Version Resumida
```
Un RIESGO es un evento que no es deseado y cuyo resultado es
perjudicial. Como tal tiene 2 caracteristicas: incierto y perjudicial.

LINEA DE CORTE es el limite que se establece en la gestion de riesgos,
entre los riesgos que van a ser monitorizados y los que seran seguidos
en segundo plano
```
## 4. Definir GCS y enumerar 3 elementos 

La **Gestion de la Configuracion del Software** es el proceso mediante
el cual se controlan los cambios en el software, identificando los
componentes que son susceptibles, registrando los cambios y las
peticiones de cambio, y controlando que los cambios se realicen en
forma correcta en todo momento en el cliclo de vida del software.

Dado que los cambios suponen riesgo y son inevitables a lo largo del
proceso de software, es necesario que sepamos gestionarlos para poder
controlarlo en todo momento.

Los elementos sujetos al control de cambio son todos los relacionados
al software, ya que es una entidad que evoluciona constantemente, mas
precisamente nos podemos referir al codigo, a los ejecutables y a la
documentacion.

El proceso de gestion de la configuracion consta de varias actividades
y etapas:

* identificacion: supone describir cada elemento de forma que podamos
  identificalos univocamente.
  
* control de version: Consta de describir las diferentes configuraciones del
  sistema para los distintos momentos. Por ejemplo los tipicos numero
  de version.
  
* control de cambio: Se trata de definir una autoridad de cambio
  quien, cuando un cambio es requerido decide si debe aplicarse o
  no. Y en caso afirmativo encausa su implementacion.

* auditoria: Una vez que el cambio fue introducido, se encarga de que
  quede en claro quien realizo el cambio, que elemetos se cambiaron,
  cuando se cambio, por que se realizo el cambio y que este hecho en
  forma correcta.
  
* Generacion de reporte: se encarga de generar los documentos
  necesarios para que el cambio quede debidamente asentado y que se
  informen a las partes interesadas.
  
### Version resumida
```
La GCS es el proceso de identificar y definir los elementos de un
sistema con la finalidad de ir controlando el cambio a lo largo de su
ciclo de vida, reportando su estado y registrando las peticiones de
cambio; verificando que en todo momento estos elementos estan
completos y son correctos.

Los elementos mencionados pueden ser:
* DOCUMENTACION
* CODIGO FUENTE
* EJECUTABLES
```

## 5. Describir 2 principios de nielsen

La interfaz de usuario es el medio a traves del cual se comunica el
sistema con el usuario. Por lo tanto el disenio de interfaces de
usuario es el proceso de construir estas interfaces. Es de suma
importancia ya que una mala interfaz puede hacer que los usuarios
cometan errores al utilizar el sistema, dificultarles su uso e incluso
generar rechazo hacia el mismo, llevandolo al fracaso.

El disenio de interfaces es un proceso en espiral que consta de cuatro
etapas:

* **Analisis y modelado** donde se establecen que elementos se veran
  reflejados en la interfaz.
  
* **Disenio de interfaz** en donde se establece que acciones podra
  realizar el usuario para cambiar el estado de la interfaz.
  
* **Construccion** de la interfaz, donde se presentan las interfaces
  tal cual las va a ver el usuario final
  
* **Validacion** de la interfaz donde se analiza como interactua el
  usuario con la interfaz.
  
Para el disenio de la interfaz podemos mencionar tres reglas de oro:

* **Dar control al usuario**, que el sistema actue acorde a las
  necesidades del usuario y que le sea de utilidad para realizar sus
  tareas.
  
* **Reducir la carga de memoria**, es decir que el usuario no tenga
  que memorizar como utilizar el software, sino que tenga una interfaz
  intuitiva, que tenga valores por defectos significativos y que la
  interfaz sea una metafora de laa realidad.
  
* **Lograr una interfaz consistente**, es decir que el usuario pueda
  realizar sus tareas y las pueda ubicar en un contexto que tenga
  algun significado.
  
En cuanto a los principios de Nielsen, son 10:

1. Comunicacion simple. La informacion presentada al usuario debe ser
   amigable, no debe tener muchas mayusculas ni abreviaciones.
   
2. Lenguaje de usuario. El lenguaje utilizado debe ser familiar para
   el usuario, no debe ser muy tecnico ni ser extranjero.
   
3. Minimizar el uso de memoria. El usuario no deberia aprender a
   utilizar una interfaz, sino que deberia resultarle intuitiva;
   agregando informacion contextual de la session, el estado y la
   navegacion.
   
4. Consistencia. No deben existir ambiguedades en el aspecto
   tecnologico ni visual del sistema. La consistencia es un aspecto
   clave para proveer fiabilidad y seguridad al sistema.
   
5. Feedback. El sistema debe proporcionar respuestas graficas o
   textuales en pantalla al usuario acerca del estado de los
   procesos. Por ejemplo, agragar validaciones, aclaraciones,
   confirmaciones... etc.
   
6. Salidas evidentes. Debe proveerse en todas las interfaces la opcion
   de salir de la actividad actual, asi como tambien volver hacia
   atras, cancelar.
   
7. Manejos de errores. Debe presentarse al usuario mensajes de error
   que no sean invasivos ni violentos. Debe explicarse la causa de
   error, las alternativas y proveer una forma de recuperarse del
   error.
   
8. Evitar el error. Debe tratar de evitar que el usuario cometa
   errores. Esto se puede lograr, limitando al cantidad de opciones de
   entrada, mostrando mensajes claros que orienten al usuario... etc.
   
9. Atajos. Debe proveerse una buena experiencia tanto para los
   usuarios novatos como para los ya experimentados, implementando por
   ejemplo atajos de teclado para satisfacer a estos ultimos.
   
10. Ayuda. Debe estar al alcance la documentacion de ayuda del
    sistema, que puede ser contextual, en linea, general... etc.

### Version resumida
```
LENGUAJE DE USUARIO: Debe tenerse en cuenta el usuario al que va
dirigido el software, enntonces utilizar lenguaje que le sea comun. Se
debe evitar utilizar lenguage muy tecnico asi tambien como la
utilizacion de terminos en otro lenguaje, excesivas siglas. La
informacion no debe ser excesiva ni deficiente. Debe ser justa.

ATAJOS: dado que puede haber usuarios que sean novatos o bien usuarios
ya experimentados, debe mantenerse una interfaz sencilla para los
primeros pero manteniendo una experiencia satisfactoria para los
ultimos. Por ejemplo agregando atajos de teclado que engloben varias
actividades en una.
```
## 6. Definir Arquitectura en capas

La arquitectura del software se encarga de definir los componentes de
un sistema y sus relaciones con el fin de que el sistema cumpla con
los requerimientos.

Por lo anterior podemos decir que es importante la eleccion de la
arquitectura porque va a incidir en si se cumplen o no los
requerimientos, mas especificamente los no funcionales.

Las arquitecturas las podemos clasificar por varias caracteristicas:
* Organizacion de los elementos: Repositorio, cliente/servidor, capas.
* Funcion de sus elementos: Agrupacion por funcionalidad, objetos.
* Modelo de control: centralizado(llamada-retorno/gestor),
  descentralizado(interrupciones, broadcast)

Dentro de las arquitecturas distribuidas contamos con: P2P,
cliente-servidor y microservicios.

La arquitectura en capas es un tipo de organizacion en donde los
distintos componentes se dividen en capas adyacentes, donde cada capa
brinda servicios a sus capas adyacentes. Este tipo de organizacion
tiene como ventaja que favorece la modularidad ya que es relativamente
facil cambiar una capa por otra siempre y cuando respeten la
interfaz; e incluso si cambia la interfaz puede agregarse una capa
intermedia que sirva de adaptador. Tambien Permite crear sistemas
multiplataforma donde las capas internas sean intercambiables y
dependan del SO host.

Las desventajas de esta arquitectura son que muchas veces es dificil
de implementar, y debido a que las capas se brindan servicios entre
si, es muy probable que los servicios que se originan en las capas de
arriba lleguen a las capas de abajo manteniendo una correspondencia en
las capas intermedias.

### Version resumida
```
Arquitectura en capas es un tipo de organizacion en donde los
componentes se dividen en capas donde cada una brinda servicios a sus
capas adyacentes.
```
## 7. Camino critico
   
# 3. Final Ingenieria 2 - Diciembre 2019

Los duenios de la cadena de tiendas de ropa para ninios "Mi primera
ropita" quieren optimizar sus procesos, para ello realizan un
analisis sobre la conveniencia de encargar un nuevo software o
mantener el actual. Finalmente se inclinan por la primera opcion y
deciden solicitar el desarrollo de un sistema de gestion a IceSoft.

En Icesoft ponen manos a la obra, y para lograr un trabajo optimo, se
decidio poner especial hincapie en la gestion de la configuracion
**[1- Describa y defina linea base]** y la planificacion. Para esto
ultimo identificaron tareas e hitos en el desarrollo 
**[2- Que es un hito]**.

Mara, lider de equipo, identifico los riesgos a cubrir y luego de
ordenarlos trazo una linea de corte.
**[3- Defina riesgo y describa linea de corte]**

En el equipo cuentan con un experto en interfaz y experiencia de
usuario; y aplicaron distintos principios de Nielsen en las diferentes
pantallas del sistema. **[4- Describa dos principios de Nielsen]**

Al momento de realizar las mediciones de complejidad y esfuerzo Mara
descarto por completo LDC y se inclino por Punto Funcion.
**[5- Describa Punto Funcion]**

En cuanto a la arquitectura decidieron utilizar Cliente-Servidor. 
**[6- Definir]**

Finalmente se planificaron las tareas y se presento al cliente la
siguiente tabla.

| TAREA | DURACION(SEM) | RESTRICCIONES                               |
| ----- | ------------- | ------------------------------------------- |
| A     |            12 |                                             |
| B     |             2 | A terminada                                 |
| C     |             4 | Empieza una semana despues de terminada B   |
| D     |             6 | A terminada. C terminada.                   |
| E     |             7 | C terminada.                                |
| F     |            10 | Empieza 6 semanas despues del comienzo de B |
| G     |             9 | C terminada. Empieza 2 semanas despues del  |
|       |               | Comienzo de E.                              |
| H     |             7 | Empieza 1 semana antes del fin de F         |
| I     |             3 | c terminada. Empieza 3 semanas despues del  |
|       |               | fin de G                                    |

Faltaria calcular el camino critico. **[7- Los ayudas?]**

## 1. Describa y defina linea base.
La GCS es el proceso de identificacion y determinacion de los objetos
de un sistema con el fin de controlar su cambio a lo largo del ciclo
de vida del software; reportando y registrando los estados de sus
componentes y las peticiones de cambio; verificando que sean completos
y esten correctamente implementados en todo momento.

Siendo el cambio un factor de riesgo y un evento inevitable en el
proceso del software, se trata de gestionarlo y controlarlo en todo
momento.

Para ello es indispensable introducir el concepto de linea
base. **Linea base** es un producto o especificacion sobre la cual se
ha llegadoa un acuerdo, que de ahi en adelante sirve como base para el
desarrollo y solo puede cambiarse a traves de procedimientos formales
de gestion de cambios.

En otras palabras linea base es un estado del sistema en un momento
dado que sirve como punto de partida para realizar el desarrollo. En
caso de tener que realizar cambios en el software, solo se podran
hacer mediante el proceso de GCS, que nos llevara a una nueva linea
base para continuar con el desarrollo.

### Version resumida
```
LINEA BASE es un producto o especificacion sobre la que se han llegado
a acuerdos, que sirve como punto de partida para futuros desarrollos y
que debe cambiarse solo mediante procedimientos formales de gestion de
cambios.

Es decir, que linea base es el punto de partida en la produccion de
software, de forma tal que para generar cambios debemos utilizar
alguna metodologia de gestion de cambios(como CGS) y entonces asegurar
que se puede seguir un historial de cambios, saber en que consistio
cada uno de ellos, que lo origino, quien lo autorizo y que esta
aplicado correctamente.
```

## 2. Que es un hito
Parte del proceso de gestion de un proyecto incluye la planificacion
temporal, que es la administracion de los esfuerzos dentro de un plazo
determinado de tiempo para que el producto final cumpla con los
requisitos.

Los principios para la planificacion temporal son:

* Dividir el proyecto en tareas y actividades manejables

* Se debe identificar las interdependencias entre tareas.

* Cada tarea debe tener una fecha de inicio y fin.

* Evitar la sobreaasignacion de tareas

* Cada tarea debe tener responsables definidos.

* Las tareas deben tener resultados definidos.

* La planificacion debe estar basada hitos definidos.

Entonces podemos definir distintos conceptos relacionados a la
planificacion temporal, como por ejemplo: **hito**.

Un hito es "algo" que se espera que este hecho para cierta fecha que
es notable(por ejemplo, se entrega una funcionalidad critica) con un
objetivo facilmente validable y facil de evaluar.

### Version resumida
```
Un hito es "algo" que se espera este hecho para cierta fecha, cuyo
objetivo es facil de validar,facil de evaluar y es notable.
Por ejemplo, se entrega una funcionalidad critica.
```

## 3. Defina riesgo y describa linea de corte.

Un riesgo es un evento no deseado cuyas consecuencias son
perjudiciales. Dicho de otra forma, son eventos que pueden ocurrir y
que nos interesa evitar, mitigar o al menos saber responder ya que
producen una perdida.
Podemos decir que los riesgos tienen dos caracteristicas:

* **Incertidumbre**, ya que no tenemos la certeza de cuando van a
  ocurrir, y ni siquiera si lo van a hacer.
  
* **Perjudicial**, ya que su ocurrencia genera perdidas, ya sea
  monetarias o de tiempo.
  
Dada su naturaleza, es importante saber gestionar los riesgos. El
proceso de gestion de riesgos consta de cuatro etapas:

- Identificacion: Se trata de enunciar todos los riesgos con su tipo.

- Analisis: En esta etapa se cuantifican tanto la probabilidad de que
  sucedan como su alcance, es decir cuan grave resultan. Habiendo
  cuantificado ambas cantidades se ordenan en base a probabilidad y
  alcance. Finalmente se establece una linea de corte, que dividira
  los riesgos que seran monitoreados activamente de los que no lo
  seran.

- Planeamiento: En esta etapa se definen estrategias para tratar cada
  riesgo. Se puede adoptar una estrategia preventiva(evitar que
  surja),  de contingencia(minimizar danios) y/o de respuesta( dejar
  que suceda y actuar en consecuencia).

- Supervision: Se trata de realizar el proceso constantemente para
  identificar nuevos riesgos, o bien ver los que ya ocurrieron y
  afinar las estrategias.
  
### Version resumida
```
Un RIESGO es un evento no deseado cuya ocurrencia es perjudicial

La linea de corte es una forma que tenemos en la gestion de riesgos
para diferenciar, de todos los riesgos identificados, aquellos que
merecen especial atencion. No hay un criterio unificado en cuanto a
donde debe establecerse, mientras que bohem plantea en forma
arbitraria 10 riesgos; otros autores plantean que depende del contexto
del prooyecto, la gente disponible, complejidad, etc.
```
## 4. Describa dos principios de Nielsen

La interfaz es el medio de comunicacion entre el hombre y el
sistema. Dado que los sistemas seran utilizados por personas, es
importante que estas interfaces esten bien diseniadas, ya que muchas
veces el fracaso de un sistema depende de la ella. Por ejemplo, una
interfaz mal diseniada puede causar rechazo en el usuario final y este
puede negarse a usar el sistema.

El disenio de interfaces es un proceso iterativo en forma es espiral
que consta de cuatro etapas:

- Analisis y modelado: se definen los objetos y acciones de la
  interfaz.
  
- disenio: se establecen que acciones podra realizar el usuario para
  cambiar el estado de la interfaz.
  
- Contruccion: se presenta la interfaz tal cual el usuario final la
  utilizara.
  
- validacion: se entrega la interfaz al usuario y se observa como este
  se desempenia con ella.
  
El disenio de interfaces como bien se menciono previamente es muy
importante, y se consideran tres reglas de oro a tener en cuenta:

* **Dar control al usuario**, que el sistema reaccione a las
  necesidades del usuario y este le ayude a completar sus tareas.
  
* **Minimizacion de memoria**, se trata de tener valores por defecto
  significactivos y que la interfaz sea una metafora de la realidad.
  
* **Consistencia** de la interfaz, brindar informacion contextual en
  todo momento de forma que el usuario pueda dar significado a las
  tareas que realiza en un contexto.
  
Los principios de Nielsen son 10:

* **Dialogo simple**, se trata de que la informacion mostrada al
  usuario sea significativa, por ejemplo que este bien redactada y no
  tenga muchas siglas.
  
* **Lenguaje comun**. Utilizar un lenguaje en comun con el usuario, no
  utilizar lenguaje muy tecnico ni abreviaciones. En otras palabras,
  dirigirse al usuario de una forma que le resulte comoda.
  
* **Minimizacion del uso de memoria**, se trata de que el usuario
  actue de forma natural con el sistema. Brindar informacion
  contextual de la sesion y navegacion para que sepa en todo momento
  donde se encuentra.
  
* **Consistencia** tanto visual como de las tecnologias utilizadas. De
  esta aumenta la fiabilidad y seguridad en el sistema.
  
* **Salidas evidentes**, se trata de brindarle en todo momento la
  posibilidad al usuario de cancelar una accion, modificarla o bien
  volver hacia atras. Esta opcion debe ser visible.
  
* **Mensajes de Error**, el manejo de los mensajes de error debe ser
  el adecuado y con esto se refiere a que los mensajes no deben ser
  intimidantes, explicar cual es la situacion de error, en que estado
  esta el sistema y brindar opciones para su rectificacion.
  
* **Evitar el error**, se trata de tratar de evitar que el usuario se
  equivoque, esto se logra por ejemplo, agregrando validaciones,
  mensajes de ayuda o incluso limitando lo que puede ingresar.
  
* **Ayuda**, el usuario debe tener a su disposicion en todo momento
  mensajes de ayuda y documentacion, que pueden ser contextuales,
  generales o incluso en linea.
  
* **Feedback**, en todo momento el usuario tiene que saber el estado
  de sus tareas. Se le debe presentar informacion ya sea textual o
  grafica que le indiquen como se esta desarrollando el proceso.
  
* **Atajos**, se trata de abarcar tanto a los usuarios novatos como a
  los experimentados; siendo que el segundo es probable que por su
  experiencia le resulte tedioso realizar ciertas tareas, se le debe
  ofrecer atajos para las mismas. De forma tal que el sistema le
  continue siendo amigable al novato, y productivo al experimentado.
  
### Version resumida
```
ATAJOS: Se trata de unificar la experiencia entre usuarios novatos y
experimentados. Es muy probable que a los usuarios experimentados que
ya conocen el sistema les resulte tediosa la interfaz si esta esta
pensada solo para los usuarios novatos. Entoces se debe ofrecer atajos
para que utilicen quienes ya conocen el funcionamiento del mismo. De
forma tal que les resulte amigable a los novatos y productivo a los
experimentados.

LENGUAJE COMUN: se trata de utilizar lenguaje adecuado para mostrar
informacion al usuario. Es decir, evitar utilizar mucho lenguaje
tecnico, evitar la utilizacion excesiva de siglas, evitar lenguaje
extranjero. En resumen, dirigirse al usuario de una forma que le
resulte natural y se sienta comodo.
```

## 5. Describa Punto Funcion

Las metricas sirven para cuantificar cuanto cumple un sistema con
cierto atributo.

Sirven para entender que sucede en el desarrollo de software,
controlar que es lo que ocurre en el proyecto, evaluar la calidad y
mejorar nuestros productos. 

Tenemos distintos tipos de metricas, entre ellas metricas de producto
y metricas de control; mientras que las primeras sirven para medir
atributos del software las segundas sirven para cuantificar atributos
del proceso de software.

Un ejemplo de metricas de control es la metrica de punto funcion que
mide la cantidad de funcionalidad que contiene una especificacion de
sistema y en base a ello se puede estimar su tamanio.

Se calcula mediante una formula que tiene en cuenta factores propios
de la organizacion y el equipo de desarrollo y es subjetivo. Y tambien
esta ponderado mediante un corficiente que calcula en base a un grupo
de preguntas.

Obteniendo el PF podemos calcular costo, produvtividad y calidad.

### Version resumida
```
Punto funcion(PF)es una metrica de control que sirve para calcular la
cantidad de funcionalidad de una especificacion de software y en base
a ello calcular su tamanio.

Esta metrica esta dada principalmente por dos coeficientes. El primero
totalmente subjetivo y que depende de las estimaciones del equipo de
desarrollo y la organizacion. El segundo se obtiene luego de responder
una serie de preguntas. 

En base al calculo de PF se puede calcular costos, calidad y productividad.
```
## 6. Definir cliente-servidor.

La arquitectura se define como la relacion entre los componentes de un
sistema con la finalidad de que logre cumplir con los requerimientos.

Podemos clasificar las arquitecturas segun varios criterios.
Segun la organizacion de sus componentes:
- Repositorio

- Cliente/servidor

- Capas

Segun la modularizacion de sus componentes:

- Funciones agrupadas

- Objetos

Segun su modelo de control:

- Centralizados
  - llamada/retorno
  - gestor
  
- orientado a eventos
  - broadcast
  - manejador de interrupciones.
  
Dentro de los sistemas distribuidos:

- P2P

- Cliente/servidor

- Microservicios

La arquitectura cliente-servidor es una arquitectura distribuida,
aunque tambien puede ser local. Consta de tres componentes:

- Servidor, es quien maneja la logica y los datos; y los ofrece a
  traves de servicios.
  
- Clientes, son quienes requieren los servicios de los servidores.

- Red, es el medio a traves del cual se realiza la comunicacion entre
  clientes y servidores.
  
Este tipo de arquitectura tiene la ventaja de que pueden establecerse
multiples servidores y no es necesario que estos conozcan a los
clientes, lo que simplifica su implementacion y la comunicacion entre
ambos.

Existen dos modelos de cliente-servidor:

- **Dos niveles:** cliente y servidor, donde este ultimo contiene
  logica y maneja los datos del sistema. El cliente puede ser a la vez
  de dos tipos, liviano(solo se encarga de presentar datos) o
  pesado(contiene logica).

- **Multinivel:** Consta de clientes livianos, servidores que manejan
  la logica y se agregan mas niveles en los cuales los servidores se
  apoyan para realizar su tarea. Como por ejemplo, manejo de bases de
  datos.
  
### Version resumida
```
Cliente/servidor es una arquitectura de software que consta
principalmente de tres elementos:

- Servidores, encargados de la logica del sistema y el manejo de los
datos; que ofrecen al exterior a traves de servicios.

- Clientes, son quienes consumen los servicios de los servidores.

- Red, es el medio a traves del cual se hace posible esta
comunicacion.

Esta arquitectura tiene como ventaja el desacople entre ambos
componentes(cliente/servidor) ya que el servidor no necesita conocer a
sus clientes, y los clientes solo necesitan conocer donde buscar los
servicios.

Esta arquitectura puede ser de dos tipos:

DOS NIVELES: Una arquitectura de dos partes, una cliente y otra
servidor. Donde la parte servidor es quien maneja la logica del
sistema y ademas es quien administra los datos. El cliente puede ser
liviano(solo presenta datos) o pesado(ademas de presentar datos
contiene logica del sistema).

MULTINIVEL: Consta de clientes livianos, servidores que se encargan de
la logica del sistema, pero estos servidores se apoyan en otros
servidores para realizar su funcion. Como por ejemplo: servidores de
bases de datos.
```
## 7. Camino critico
inicio->a->b->c->g->i->fin

# 4. Final ingenieria 2 - Primera fecha febrero 2020
## 1. Gestión de configuración de software que es y describir el proceso
La GCS es la tecnica que se utiliza para controlar y gestionar el
cambio en los sistemas. Dado que el cambio es un factor de riesgo y es
inevitable es importante saber manejarlo.

Mas especificamente la gestions de configuracion consiste en
identificar y definir los elementos de un sistema para controlar su
cambio a lo largo de todo el ciclo de vida; registrando su estado y
las solicitudes de cambio; verificando en todo momento que esten
completos y sean correctos.

El proceso de de configuracion del software consta de varias etapas
compuestas por diferentes tareas cada una:

1. Identificacion. Se identifican los elementos que son susceptibles
   de cambio y se establece una forma de identificar cada uno
   univocamente.
   
2. Control de version: se trata de mantener un historial de las
   diferentes configuraciones que se van dando a lo largo del tiempo.
   Una forma de llevar a cabo esta tarea es la utilizacion de
   herramientas de versionado, como git o svn. Tambien se puede
   adoptar tecnicas como estandares de numeracion de versiones con
   versiones mayores, menores y fixes.
   
3. Control de cambio: para llevar a cabo esta etapa, se debe primero
   definir una autoridad de central de cambio(ACC) que es la encargada
   de definir que cambios se aplican, que elementos se veran
   involucrados y designar a las personas que estaran a cargo del
   cambio.
   
   Esta etapa involucra varias tareas. El cambio primero comienza con
   una solicitud. Esta solicitud es evaluada por algun desarrollador
   que la analiza y la eleva a la ACC. La ACC es entonces quien decide
   si el cambio debe aplicarse o no. En caso negativo se responde a
   quien solicito el cambio explicando que no se llevara a cabo el
   cambio. En caso positivo realiza un registro de cambio donde
   establece el cambio que se va a realizar e identifica que elementos
   cambiaran. Una vez que se realiza el cambio en los elementos, se
   aplican y se establece una nueva linea base lista para ser
   aplicada.
   
4. Supervision: Una vez que se ha aplicado el cambio se genera una
   auditoria del cambio que controla que se hayan cumplido los pasos
   formales para aplicar el cambio. Se genera la documentacion
   asociada y se notifica a las partes interesadas del nuevo cambio
   implementado.
   
### Version resumida
```
La GCS es el proceso de gestion de los cambios del software que se
encarga de identificar y definir los elementos que seran sujeto de
cambio a lo largo del ciclo de vida del sistema, registrando y
reportando los cambios en los elementos y las peticiones de cambio;
validando en todo momento que los objetos esten completos y que los
cambios sean correctos.

El proceso de gestion de la configuracion del software se basa en
varias etapas con distintas tareas cada una, que se encarga de
identificar los elementos y los cambios que ocurren respondiendo que
cambios se realizan, que elementos cambiaron, quien fue el encargado
de implementarlos y si se realizaron correctamente. Estas etapas son:

- identificacion: Se definen los objetos y la forma de reflejar los
cambios.

- Version de control: Se definen la forma en que se reflejaran las
distintas configuraciones a lo largo del tiempo.

- Control del cambio: Se define una autoridad central del cambio cuya
responsabilidad es, en mayor medida, autorizar o denegar los cambios y
administrarlos.

- Supervision y auditoria: Donde se controla que los cambios se
realizaron en forma correcta, que las partes han sido notificadas y
que se genero la documentacion necesaria.
```

## 2. Que es un riesgo y definir línea de corte
Un riesgo es un evento no deseado que genera consecuencias
perjudiciales. Es importante gestionar los riesgos porque reducen la
probabilidad de que un proyecto fracase. Como tales tienen dos
caracteristicas principales: Incertidumbre, porque no es 100% seguro
que ocurran; y perjudicial porque de ocurrir generaran una perdida ya
sea en dinero o tiempo.

Linea de corte es una division que se realiza en el proceso de gestion
de riesgos en donde se define que riesgos seran monitorizados
activamente durante el desarrollo y cuales, aunque determinados, no
seran activamente gestionados.

El proceso de gestion de riesgos consta de enumerar los riesgos y
determinar su tipo(proyecto, producto, negocio..), una vez enumerados
se cuantifica su probabilidad de ocurrencia y su implicancia, es decir
que perdidas suceden. Con estos valores se ordenan los riesgos y se
traza la linea de corte. Con la linea de corte trazada se toman los
riesgos que quedan sobre la linea de corte y se planea la estrategia
con la que se trataran, que puede ser de contencion, minimizacion o
evasion; tratar el riesgo cuando ocurra, minimizar los costos cuando
ocurra o evitar que ocurra respectivamente. Finalmente se hace
supervision a lo largo del proyecto donde se revisa constantmente como
son tratados los riesgos, si aparecen nuevos o bien si se debe
realizar una nueva planificacion del riesgo.

### Version resumida
```
Un RIESGO es un evento no deseado que tiene consecuencias
perjudiciales.

LINEA DE CORTE es el limite que se establece para planificar o no un
riesgo. No hay un criterio establecido, bohem plantea arbitrariamente
la linea en el decimo riesgo; otros autores establecen que depende de
la naturaleza del proyecto(complejidad, personal disponible).
```
## 3. Que es juicio experto (técnica de estimación)
La estimacion no es una ciencia exacta, justamente se trata de
aproximar un valor, que puede ser un costo o un recurso, en base mas
que todo a experiencias previas. Como tal, podemos sostener que
cuantas mas estimaciones ha realizado una organizacion, mas ha
aprendido a estimar y se espera que se vuelva mas certera en esta
tarea.

Los cambios en los requerimientos hacen peligrar las estimaciones.

Generalmente para estimar se suele utilizar varias tecnicas y en base
a ello se realiza una estimacion final. Dichas tecnicas son:

- **Juicio experto:** se trata de preguntar a distintos expertos en la
  materia y se obtiene una estimacion en base a su palabra.
  
- **Tecnica delphi:** similar al juicio experto se reune un grupo de
  profesionales especializados y se van tomando turnos para realizar
  una estimacion. Finalizada la ronda discuten acerca de las
  estimaciones y realizan una nueva estimacion. Esto se repite hasta
  que llegan a un acuerdo.
  
- **Division del trabajo:** se trata de realizar una estimacion
  jerarquica hacia arriba. Donde las partes mas bajas hacen una
  estimacion y a medida que se va subiendo en la jerarquia la
  estimacion va ampliando el espectro.

**Tecnicas COCOMO** son una forma de realizar estimaciones en forma
empirica. COCOMO(desarrollado por bohem) relaciona el tamanio de un
producto con los costos.
COCOMO84 utiliza tres nivees de producto: orgnaico, semi-acoplado y
acoplado; segun el equipo sea pequenio y el proyecto sea aislado hasta
proyectos grandes y con grandes restricciones.

COCOMOII surge como mejora de COCOMO84. Reconoce que estimar el
tamanio de un sistema en base a sus lineas de codigo de manera
temprana es dificil. Considera diferentes enfoque en el desarrollo de
forma que se pueda estimar en forma incremental.

### version resumida
```
La estimacion consiste en predecir ciertos valores como puede ser
costos, utilizacion de recursos, tiempos. Como tal no es una ciencia
exacta y depende mucho de la experiencia previa y del acceso al
historial.

El juicio experto es una tecnica de estimacion que se basa justamente
en eso, la experiencia. Consiste en consultar a varios expertos y en
base a sus estimaciones calcular la prediccion final.
```
## 4. Describa 2 principios de Nielsen
El disenio de interfaces es muy importante porque una mala interfaz es
uno de los factores de fracaso de los sistemas. Una mala
implementacion de la interfaz puede hacer que el usuario cometa
errores e incluso puede generar rechazo a su utilizacion por parte del
usuario.

El disenio de interfaces se basa en 3 principio llamados "de oro":

- **Dar control al usuario:** el sistema debe responderle al usuario y
  serle de utilidad en las tareas que realiza.
  
- **Minimizar el uso de la memoria:** la interfaz debe resultarle
  amigable y facil de usar al usuario. Esto se logra utilizando
  haciendo que la interfaz sea una metafora de la realidad.
  
- **Consistencia:** se trata de mantener una linea ya sea en lo visual
  como en lo tecnologico que transmita al usuario fiabilidad y
  seguridad.
  
Los principios de Nielsen son 10:

1. Dialogo simple y sencillo: Se trata de que la informacion este
   correctamente escrita, que no haya exceso de abreviaciones y que se
   encuentre en su medida justa(ni mucho ni poco).
   
2. **Lenguaje comun:** Se trata de utilizar lenguaje que reconozca y
   utilice el usuario para lograr una comunicacion efectiva. Se debe
   evitar utilizar lenguaje extranjero, asi como tambien evitar
   utilizar mucho lenguaje tecnico y siglas.
   
3. **Minimizar el uso de la memoria:** Se trata de que el usuario se
   le facilite el uso de la interfaz y que no tenga que aprendersela
   de memoria. Esto se logra haciendo que la interfaz sea basicamente
   una metafora de la realidad.
   
4. **Feedback**
5. **Salidas evidentes**
6. **Mensajes de error:**
7. **Manejo de erroes:**
8. **Ayuda**
9. **Atajos**
10. **consistencia**

### Version resumida
```
MENSAJES DE ERROR: Los mensajes de error deben ser adecuados, esto
quiere decir que deben estar bien redactados, no ser
intimidatorios. Deben indicar que ha ocurrido un error, la causa que
lo genero y deben ofrecer alternativas para recuperar el estado luego
del error.

LENGUAJE COMUN: Debe utilizarse un lenguaje que le sea identificable
al usuario para que la comunicacion sea eficaz. No debe utilizarse un
lenguaje muy tecnico, lenguaje extranjero ni siglas.
```
## 5. Describir arquitectura en capas
La arquitectura de software se encarga de como se dividen los
componentes de un sistema y como interactuan para que este logre
cumplir los requerimientos.

Es importante definir una arquitectura que es adecuada al sistema
porque como bien se menciono arriba, la arquitectura va a impactar en
como logra el sistema cumplir con los requerimientos, mas
especificamente los requerimientos no funcionales, como rendimiento y
seguridad, por ejemplo.

Las Arquitecturas pueden ser de muchos tipos, y se organizan segun
diferentes criterios. Segun la organizacion de los componentes podemos
mencionar tres arquitecturas:
- Repositorio
- Cliente/servidor
- Arquitectura en capas.

La arquitectura en capas se caracteriza porque sus componentes se
distribuyen en capas, en donde cada una provee servicios a sus capas
adyacentes.

Tiene como ventajas principales que es muy facil intercambiar una
capa, basta con que cumpla la interfaz de servicios que provee; e
incluso en caso de no hacerlo se puede implementar una capa intermedia
en forma de adaptador. Tambien es posible construir sistemas
multiplataforma intercambiando unicamente las capas inferiores que
interactuan con el sistema operativo.

Como desventaja podemos mencionar que es dificil de modelar. Ademas
aquellos servicios que se resuelven en las capas inferiores es muy
probable que tengan que atravesar todas las capas intermedias
partiendo de las superficiales, lo que implica que haya una
correspondencia en cada capa.

### version resumida
```
La ARQUITECTURA EN CAPAS se caracteriza por ordenar los componentes
del sistema en capas, las cuales brindan(y consumen) servicios a(de)
las capas adyacentes.
```
## 4. Definir caja negra
Las pruebas son importantes porque, aunque los errores se pueden
detectar en etapas tempranas como en la especificacion o en el
disenio, permiten encontrar errores que pueden haber sido pasados por
alto en estas etapas.

Los hay de dos tipos: 
- Errores por omision(falta de inicializacion de una variable, falta
  de definicion de una funcion... etc)
  
- Defecto de cometido: aquellos que surgen de alguna implementacion
  incorrecta(formula mal usada)
  
Para encontrar errores, se utilizan dos tecnicas:

- Caja blanca: donde se prueba el sistema teniendo en cuenta su
  implementacion. Es decir, probando sus estructuras internas.
  
- Caja negra: Donde no se prueba la implementacion, sino que se prueba
  la interfaz. Es decir, se prueban las entradas y las salidas.
  
Para la prueba de caja negra es importante el concepto de clase de
equivalencia, donde para cada parametro de definen conjuntos de datos
validos e invalidos. Entonces se prueba en base a estos conjuntos en
general.

Tambien como complemento a estas pruebas se utiliza el AVL(analisis
del valor limite) que parte de la base que los errores no suelen
encontrarse en el centro de los valores, sino mas bien en los
extremos; entonces si tenemos un rango de valores a<x<b, se prueba en
a, b y un valor representativo del rango.

Las pruebas de caja negra, tambien llamadas pruebas de comportamiento,
se centran en encontrar defectos en la interfaz, Funciones incorrectas
o ausentes, errores de rendimiento, errores en acceso a las bases de
datos... Concentra las pruebas en el dominio de la informacion.

### Version resumida
```
Las pruebas de caja negra, o pruebas de comportamiento, se encargan de
probar la interfaz del sistema. Buscando errores como por ejemplo en
funciones que son incorrectas o inexistentes, errores en las bases de
datos, errores de rendimiento...
```
## 5. Hacer un camino crítico
(sin enunciado)
# 5. Final Ingenieria 2 - (Segunda) Febrero 2020

Contrucciones Dry es una empresa dedicada a la construccion en seco de
la ciudad de La Plata, contrato a la empresa SoftLp para que
desarrolle un sistema.

En la empresa SoftLp aceptaron el trabajo, y dada las caracteristicas
del desarrollo decidieron hacer especial incapie en la gestion de la
configuracion.
**[1- Describa el proceso de Gestion de la Configuracion]**

El grupo de trabajo esta conformado por un lider de proyecto(Jose),
quien decidio aplicar los principios del modelo MOI a su equipo de
trabajo. Luego realizaron el analisis de riesgos 
**[2- Describa la gestion de Riesgos]** y plantearon diferentes planes
de minimizacion y contingencia. Al momento de realizar las mediciones
de compejidad y esfuerzo, Jose descarto por completo las metricas y se
inclino por las estimaciones. **[3- Describa Juicio de Expertos]**

El equipo cuenta con un experto en interfaz y experiencia de usuario
quien decidio aplicar distintos principios de Nielsen en las
diferentes pantallas del sistema. 
**[4- Describa 2 principios de Nielsen]**

En cuanto a la arquitectura decidieron utilizar una en capas 
**[5- Describir]** y decidieron hacerse cargo del mantenimiento.
**[6- Describir tipos de mantenimiento]**

Para implementar el sistema se estimaron las actividades que se deben
llevar a cabo con el tiempo en dias.
**[7- Encontrar el camino critico, calculando tiempos tempranos y
tardios. Calcular el tiempo final]**

| Actividad | Precedencia | Tiempo mas probable(dias) |
| ----------| ----------- | ------------------------- |
| A         | -           |                         2 |
| B         | -           |                         3 |
| C         | A           |                         4 |
| D         | A,C         |                         3 |
| E         | A           |                         4 |
| F         | E           |                         2 |
| G         | D,F         |                         1 |
| H         | D,F         |                         5 |
| I         | G           |                         2 |
| J         | G,H,I       |                         3 |
| K         | H,I,J       |                         4 |

