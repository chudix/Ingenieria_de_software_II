<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [SRS(Software Requirement Specification)](#srssoftware-requirement-specification)
    - [Introduccion](#introduccion)
    - [Especificacion(Elicitacion)](#especificacionelicitacion)
        - [Tecnicas de elicitacion](#tecnicas-de-elicitacion)
    - [Requerimiento](#requerimiento)
        - [Requerimientos Funcionales](#requerimientos-funcionales)
        - [Requerimientos No Funcionales](#requerimientos-no-funcionales)
    - [Especificacion(Documentos/Estandares)](#especificaciondocumentosestandares)
        - [Descripcion del sistema(ConOps - "CONcepts of OPerationS" - IEEE1362)](#descripcion-del-sistemaconops---concepts-of-operations---ieee1362)
        - [SRS (Software Requirements Specification - ERS - IEEE830)](#srs-software-requirements-specification---ers---ieee830)
            - [Introduccion](#introduccion-1)
            - [Descripcion General](#descripcion-general)
            - [Requerimientos especificos](#requerimientos-especificos)
            - [Apendice](#apendice)
- [Gestion de la Configuracion del Software(GCS)](#gestion-de-la-configuracion-del-softwaregcs)
    - [Definicion](#definicion)
    - [Linea base](#linea-base)
    - [Proceso GCS](#proceso-gcs)
        - [Identificacion](#identificacion)
        - [Control de versiones](#control-de-versiones)
        - [Control de cambios](#control-de-cambios)
        - [Auditoria](#auditoria)
        - [Generacion de Informes](#generacion-de-informes)
- [Gestion de Proyectos](#gestion-de-proyectos)
    - [Planificacion](#planificacion)
        - [Planificacion temporal](#planificacion-temporal)
        - [Planificacion organizativa](#planificacion-organizativa)
            - [Lideres de equipo (Gestores tecnicos)](#lideres-de-equipo-gestores-tecnicos)
            - [Equipos de desarrollo(Profesionales)](#equipos-de-desarrolloprofesionales)
- [Riesgos](#riesgos)
    - [Gestion de riesgos](#gestion-de-riesgos)
    - [El proceso de gestion de riesgos](#el-proceso-de-gestion-de-riesgos)
        - [Identificacion de riesgos](#identificacion-de-riesgos)
        - [Analisis de riesgo](#analisis-de-riesgo)
        - [Planeacion de riesgo](#planeacion-de-riesgo)
        - [Supervision de riesgo](#supervision-de-riesgo)
- [Disenio de Interfaz de Usuario](#disenio-de-interfaz-de-usuario)
    - [Proceso de Disenio](#proceso-de-disenio)
        - [UdX/UX](#udxux)
    - [Estilo de Interfaces](#estilo-de-interfaces)
    - [Aspectos del disenio](#aspectos-del-disenio)
        - [Principios de Nielsen](#principios-de-nielsen)
        - [Presentacion de la informacion](#presentacion-de-la-informacion)
        - [Soporte al usuario](#soporte-al-usuario)
    - [Interfaces inteligentes](#interfaces-inteligentes)
    - [Usabilidad](#usabilidad)
- [Metricas](#metricas)
    - [Metricas de producto](#metricas-de-producto)
    - [Metricas de control](#metricas-de-control)
    - [GQM(Goal Question Metric)](#gqmgoal-question-metric)
- [Estimaciones](#estimaciones)
    - [Costos](#costos)
    - [Recursos](#recursos)
    - [Tecnicas de estimacion](#tecnicas-de-estimacion)
    - [Modelos Empiricos de Estimacion (COCOMO)](#modelos-empiricos-de-estimacion-cocomo)
- [Disenio de software](#disenio-de-software)
    - [Disenio de datos](#disenio-de-datos)
    - [Disenio de Arquitectura](#disenio-de-arquitectura)
    - [Disenio a nivel componentes](#disenio-a-nivel-componentes)
    - [Disenio de interfaces](#disenio-de-interfaces)
    - [Criterios tecnicos para la evaluacion de un buen disenio](#criterios-tecnicos-para-la-evaluacion-de-un-buen-disenio)
    - [Conceptos de disenio](#conceptos-de-disenio)
        - [Abstraccion](#abstraccion)
        - [Arquitectura](#arquitectura)
        - [Patrones](#patrones)
        - [Modularidad](#modularidad)
        - [Ocultamiento de informacion](#ocultamiento-de-informacion)
        - [Independencia funcional](#independencia-funcional)
        - [Refinamiento](#refinamiento)
        - [Refactoring](#refactoring)
- [Arquitecturas](#arquitecturas)
    - [Disenio Arquitectonico](#disenio-arquitectonico)
        - [Organizacion del sistema](#organizacion-del-sistema)
            - [Repositorio](#repositorio)
            - [Cliente-Servidor](#cliente-servidor)
            - [Arquitectura en Capas](#arquitectura-en-capas)
        - [Descomposicion modular](#descomposicion-modular)
        - [Modelos de control](#modelos-de-control)
            - [Control centralizado](#control-centralizado)
            - [Control manejado por eventos](#control-manejado-por-eventos)
    - [Sistemas distribuidos](#sistemas-distribuidos)
        - [Multiprocesador](#multiprocesador)
        - [Cliente-Servidor](#cliente-servidor-1)
        - [Componentes distribuidos](#componentes-distribuidos)
        - [Computacion distribuida interorganizacional](#computacion-distribuida-interorganizacional)
        - [Peer to Peer(P2P)](#peer-to-peerp2p)
        - [Arquitectura orientada a servicios](#arquitectura-orientada-a-servicios)
- [Pruebas](#pruebas)
    - [Enfoque estrategico de pruebas](#enfoque-estrategico-de-pruebas)
    - [Tipos de pruebas de software](#tipos-de-pruebas-de-software)
        - [Pruebas de unidad](#pruebas-de-unidad)
        - [Pruebas de integracion](#pruebas-de-integracion)
        - [Pruebas del sistema](#pruebas-del-sistema)
        - [Pruebas de validacion](#pruebas-de-validacion)
    - [Depuracion](#depuracion)
    - [Prueba en entornos especializados](#prueba-en-entornos-especializados)

<!-- markdown-toc end -->

# SRS(Software Requirement Specification)
## Introduccion

>Un *proceso de software* es un conjunto de tareas y resultados asociados
>que producen un producto de software.

Sus actividades pueden agruparse en cuatro grandes grupos:

1. *Especificacion* de software.

2. *Desarrollo* de software.

3. *Validacion* de software.

4. *Evolucion* de software.

## Especificacion(Elicitacion)

>La elicitacion de requisitos es el proceso de adquirir toda la
>informacion relevante y necesaria para para producir un modelo de
>requerimientos del sistema a desarrollar con el fin de conocer las
>necesidades tanto implicitas como explicitas del cliente y los
>usuarios finales, entender el dominio del problema y comprender
>cualquier otro sistema preexistente ya sea manual o informatizado.

### Tecnicas de elicitacion

* Muestreo de la documentacion, datos y formularios existentes.

* Investigacion y visitas al sitio.

* Observacion del ambiente de trabajo.

* Cuestionarios

* Entrevistas

* Planeacion conjunta de requerimientos(JPR o JAD)

* Luvia de ideas

## Requerimiento

Los requerimientos son funcionalidades que debe cumplir el software
para cumplir su proposito.

Tiene seis caracteristicas(CCCNNV):

- _Conciso_: Facil de leer y entender.

- _Completo(atomico?)_: No necesita ampliarse.

- _Consistente_: No entra en contradiccion con otro requerimiento.

- _Necesario_: Su omision genera una deficiencia.

- _No Ambiguo_: Tiene una sola implementacion.

- _Validable_

Definir requerimientos no es algo sencillo, debido a que pueden
provenir de muchas fuentes(usuarios, clientes, analistas,
diseniadores... etc) ademas de que pueden estar interrelacionados,
pueden cambiar a lo largo del desarrollo, etc. 

Ademas de no ser sencillo es importante que la definicion de los
requerimientos sea la correcta, ya que en caso de requerimientos
deficientes vamos a tener los siguientes problemas:

- _Estimacion y planificacion_: Es muy dificil calcular el tiempo y
  esfuerzo que demandara algo que no se conoce.

- _Disenio_: Errores en requerimientos impactaran en la arquitectura
  que pasado un tiempo se probara que es erronea y se debera
  modificar.

- _Construccion_: Deficiencia en los requerimientos llevaran a
  desarrollar en ciclos de prueba y error que no hacen mas que
  malgastar recursos.

- _Validacion_: Con requerimientos mal definidos o incluso
  inexistentes, sera muy dificil validar el producto con el cliente a
  la hora de su entrega.

Una forma de llegar a buenos requerimientos es la de adoptar practicas
que conocemos que sirven para la finalidad y en donde la comunicacion
tiene un rol principal en la mayoria. Por ejemplo: Llegar a acuerdos
entre el equipo de desarrollo, cliente y usuarios acerca del trabajo
que debe realizarse y como se validara; tambien se debe estimar los
recursos en forma objetiva y ser eficiente en la utilizacion de los
recursos como asi tambien cumplir con atributos de calidad como
ergonomia, mantenibilidad, etc.

Existen dos tipos de requerimientos:

### Requerimientos Funcionales

Son todos aquellos requerimientos que hacen alusion a lo que el
sistema debe hacer para ser valido. Al definir un requerimiento
funcional es importante no ser ni muy general ni muy especifico.

### Requerimientos No Funcionales

No hacen referencia a lo que el sistema debe cumplir para ser valido,
mas bien se refieren a cuestiones anexas que son
deseables. Normalmente implican restricciones, como por ejemplo el
tiempo de respuesta del sistema, o que se amigable al usuario... 


## Especificacion(Documentos/Estandares)

Como bien vimos anteriormente, para lograr un buen producto de
software es muy importante no solo definir los requerimientos sino que
tambien se debe hacer de manera correcta.

Para que esto pueda realizarse existen dos documentos:

### Descripcion del sistema(ConOps - "CONcepts of OPerationS" - IEEE1362)

Es un documento dirigido a los usuarios y donde desde el punto de
vista de estos se describen las caracteristicas generales del sistema.

Es el medio de comunicacion que recoge la vision general del sistema
compartida entre el equipo de desarrollo y el cliente.

El estandar 1362 no provee tecnicas exactas para la confeccion del
documento, es una guia de referencia en donde se establecen que partes
son necesarias. Las mas importantes son las partes 3 y 4, *Descripcion
de sistemas preexistentes* y *Descripcion del sistema propuesto*
respectivamente.

### SRS (Software Requirements Specification - ERS - IEEE830)

El SRS es un documento definido en el estandar IEEE830 en donde se
establece que debe evitarse incluir requerimientos de disenio o de
proyecto.

Entre los aspectos basicos que debe cubrir podemos mencionar la
funcoinalidad(lo que el software debe hacer), las interfaces
externas(como debe iteractuar el sistema con el hardware o las
personas), rendimiento, atributos de calidad y restricciones de
disenio en la implementacion.

#### Introduccion

- _Proposito_: Se define a el proposito y a quien va dirigido el
  documento.

- _Alcance_: Se le da un nombre al sistema, se define que hara y que
  no hara y se describen beneficios, objetivos y metas del sistema.

- _Definiciones, siglas y abreviaciones_

- _Referencias_: Una lista ordenada de todas las referencias de los
  documentos mencionados o utilizados para escribir el SRS.

#### Descripcion General

Esta seccion describe los factores generales que afectan el producto y
sus requerimientos. No hace una declaracion explicita de los
requerimientos, aunque hace una mencion general para ayudar a
entenderlos.

- _Perspectiva del producto_: si es autonomo o parte de un sistema mas
  grande.

- _Funciones del sistema_: un resumen a grandes rasgos de las
  funcionalidades del futuro sistema.

- _Caracteristicas del Usuario_: Quien va a utilizar el sistema.

- _Restricciones_: interfaces, politicas de la empresa, limitaciones
  del hardware, etc.

- _Suposiciones y Dependecias_: Se mencionan aquellos factores que si
  cambian pueden afectar los requerimientos.

- _Evoluciones previsibles_: se identifican los requerimientos que se
  implementaran en el futuro.

#### Requerimientos especificos

Debe contener todos los requerimientos de software a un nivel de
detalle de tal forma que le permita a los diseniadores diseniar un
sistema que satisfaga dichos requerimientos y a los auditores poder
probar que el sistema los cumple.

Estos requisitos son detallados extensivamentes y se pueden dividir
en:

- Requerimientos comunes de interfaces: descripcion detallada de todas
  las entradas y salidas del sistema.

  - Interfaz de usuario(pantallas)

  - Interfaz de hardware(configuraciones)

  - Interfaces de software(si se debe integrar con otros sistemas)

  - Interfaces de comunicacion

- Requerimientos funcionales(por ejemplo: Historias de Usuario)

- Requerimientos no funcionales
  
  - Rendimiento

  - Seguridad

  - Disponibilidad

  - Mantenibilidad

  (en general atributos de calidad)

- Otros requerimientos

  - Requerimientos legales

  - Requerimientos culturales

#### Apendice

Puede contener cualquier informacion relevante para el SRS pero que no
debe formar parte del documento.

# Gestion de la Configuracion del Software(GCS)

## Definicion

>La gestion de la configuracion del software(GCS) es el proceso de
>identificar y definir los elementos del sistema, controlando el cambio
>de estos a lo largo de su ciclo de vida, registrando y reportando el
>estado de los elementos y las solicitudes de cambio, y verificando que
>los elementos esten completos y sean los correctos en todo momento.

*[Refactorizacion]*

>La gestion de la configuracion del software(GCS) es el proceso de
>identificar y definir los elementos del sistema controlando el cambio
>de estos a lo largo de su ciclo de vida. Este proceso lleva a cabo
>varias actividades en donde se registra y reporta el estado de los
>elementos y las solicitudes de cambio; verificando que los elementos
>esten completos y sean correctos en todo momento.

Con elementos nos referimos basicamente a Programas(codigo y
ejecutables), Documentos y Datos.[Recordar definicion Software]
A lo largo del desarrollo estos elementos cambian constantnemente y la
GCS se encarga de que el cambio tenga un control exhaustivo. Es decir,
la GCS se encarga del manejo de dichos cambios, que son inevitables a
lo largo del ciclo de vida del software.

## Linea base

Es un concepto que nos ayuda a controlar los cambios. Segun la IEEE:

>Una especificacion o producto que se ha revisado formalmente y sobre
>el que se ha llegado a un acuerdo, que de ahi en adelante sirve para un
>desarrollo posterior y que puede cambiarse solamente a traves de
>procedimientos formales de control de cambio.

## Proceso GCS

La importancia de la GCS se da en el contexto de la identificacion y
manejo de los cambios del software dentro de una
organizacion. Basicamente se trata de proveer luz sobre estos cambios
y responder a una serie de preguntas, como por ejemplo:

- Como identifica y gestiona una organizacion las diferentes versiones
  de un programa(documentacion incluida) de forma que se puedan
  introducir cambios eficientemente?

- Como controla los cambios antes y despues que el software sea
  distribuido al cliente.

- Quien tiene la responsabilidad de aprobar y dar prioridad a los
  cambios?

- Como podemos garantizar que los cambios se han llevado a cabo
  correctamente?

- Que mecanismo se usa para avisar a otros de cambios realizados?

Para poder responder a todas estas preguntas es que se divide el
proceso en distintas actividades. Las cuales son:

### Identificacion

Se refiere a la identificacion de los objetos en la GCS. Para ello se
utiliza:

- Nombre: Un conjunto de caracteres que lo identifican univocamente.

- Descripcion: Tipo de ECS(datos, codigo, documentacion),
  identificador del proyecto, informacion de la version.

### Control de versiones

Permite al usuario especificar las configuraciones alternativas del
sistema mediante la seleccion de versiones adecuadas de ECS. Ej.

Un programa contiene los modulos 1,2,3,4,5

V.A => contiene los modulos 1,2,3,4
V.B => contiene los modulos 1,5,3

Basicamente se trata del sistema de versionado.. como Git o svn.

### Control de cambios

Dado que los cambios son inevitables a lo largo del ciclo de vida del
software, se trata de controlar como esos cambios suceden.

Se combinan procedimientos humanos con herramientas adecuadas para la
incorporacion de cambios.

Surge la figura de Autoridad de Control de Cambios(ACC) que es el
responsable de analizar los cambios y autorizar o denegar los mismos.


A grandes rasgos el control de cambios podria verse como:

Usuario propone un cambio => 

Desarrollador lo evalua => 

Desarrollador eleva informe de cambio a la ACC => 

ACC decide si el cambio se aplica o no => 

-> si no se aplica se informa al usuario

-> si se aplica se genera un peticion

-> se asignan ECS

-> se produce cambio

-> se prueba en nueva linea base.

-> promocion de cambios en prox revision

-> revision de los cambios en todos los ECS(Auditoria)

-> Contruccion de nueva version

-> distribucion

### Auditoria

Hasta ahora ya sucedieron la identificacion, el control de versiones y
la gestion del cambio que ayudan al equipo de desarrollo a mantener un
orden hasta que se genera la peticion de cambio. Pero como se
garantiza que los cambios introducidos estan correctamente aplicados?
Mediante revisiones tecnicas formales y Auditorias de la
configuracion.

Las auditorias responden mas que todo a si se realizo el cambio de la
peticion, si se incluye otro cambio, si estos cambios estan
correctamente reflejados en cada ECS y si se siguieron buenas
practicas ingenieriles.

### Generacion de Informes

Por ultimo, esta actividad se encarga de responder:

- que paso?

- quien lo genero?

- Cuando paso?

- Quien mas se vio afectado?

De esta forma cada cambio queda debidamente reportado y se avisado a
las partes involucradas.


# Gestion de Proyectos

>Un _proyecto_ es un esfuerzo temporal que se lleva a cabo para crear un
>producto, resultado o servicio unico.

Los aspectos que cubren la gestion de proyectos son:

- _Personal(RRHH)_: es el elemento mas importante. Identifica los
  interesados, determina sus requisitos y expectativas; y gestiona su
  influencia con los requisitos para garantizar el exito del proyecto.

- _Producto_: El software. Dado que es un bien intangible puede ser
  dificil ver su progreso.

- _Procesos_: El proceso de software ofrece un marco de trabajo sobre el
  cual se puede establecer un plan detallado para el desarrollo de
  software.

- _Proyecto_: Los proyectos deben ser planeados y controlados para
  manejar su complejidad.

Vale aclarar que la gestion de proyectos abarca *todo* el proceso de
software y como resultado de una mala gestion se producen perdidas que
en su mayor parte son economicas. 

Dentro del la gestion de proyectos nos encontramos con una etapa que
se denomina planificacion que abarca el calendario temporal del
proyecto y la organizacion del personal.

## Planificacion

La planificacion se encarga de responder las preguntas:

- Que debe hacerse?

- Con que recursos?

- En que orden?

Hay dos tipos de planificacion:

### Planificacion temporal

>Es una actividad que distribuye el esfuerzo estimado a lo largo de la
>duracion prevista del proyecto.

La planificacion temporal tiene base en la duracion prevista del
proyecto. Podemos mencionar dos perspectivas de dicha planificacion:

- Fecha establecida por el cliente. El equipo se ve obligado a
  distribuir el esfuerzo en ese plazo.

- Fecha establecida por los desarrolladores luego de un cuidadoso
  analisis en donde se busca uso optimo de recursos.

La planificacion temporal se basa en 5 principios:

- _Compartimentacion_: dividir el proyecto en tareas y actividades
  manejables.

- _Interdependencia_: Determinar la interdependencia de cada
  tarea. Identificar cuales pueden realizarse en paralelo y cuales no.

- _Asignacion de tiempo_: Una fecha inicial y final.

- _Validacion de esfuerzo_:  Evitar la sobreasignacion.

- _Responsabilidades definidas_: Asignar responsabkes a cada tarea.

- _Resultados definidos_: Cada tarea debe tener un resultado.

- _Hitos definidos_


Basicamente la planificacion temporal se compone de tareas o hitos con
un resultado bien definido que se deben completar para finalizar el
proyecto.

Existen diferentes metodologias para su aplicacion.

- PERT(Program Evaluation and Review Technique). Desarrollado por el
  gobierno de estados unidos. Consta de una red de tareas con tiempo
  temprano y tardio; y un camino critico.

- CPM(critical path method). se utiliza en proyectos en donde hay poca
  incertidumbre en las estimaciones.

- Diagramas de Gantt. Se representan las tareas sobre una escala de
  tiempo donde el largo de cada tarea es proporcional al tiempo.

- PERT+CMP. Actualmente conocido como el metodo del camino critico,
  combina lo mejor de los metodos.

Con el metodo del camino critico podemos calcular cuanto tiempo se
puede retrasar una tarea, si va a impactar en nuestra planificacion y
actuar en base a eso.


### Planificacion organizativa
    
La planificacion organizativa hace referencia a la administracion del
personal. Al ser el activo mas importante de una organizacion, su
mala administracion termina siendo un factor principal del fracaso de
los proyectos.

En su desarrollo se ven involucrados:

- _Gestores Ejecutivos_: Definen temas empresariales.

- _Gestores Tecnicos_: Planifican, motivan y organizan a los
  profesionales.
  
- _Profesionales_: Aportan su conocimiento tecnico.

- _Clientes_: Especifican los requerimientos.

- _Usuarios_: Interactuan con el software.

#### Lideres de equipo (Gestores tecnicos)

Normalmente se busca que los lideres de equipo cumplan con el modelo
MOI:

- **M**otivacion al personal: deben ser capaces de alentar al equipo a
  su cargo para mantener la produccion al maximo.
  
- **O**rganizacion: Deben saber modelar procesos para que el concepto
  inicial se transforme en un producto final.
  
- **I**ncentivacion de ideas e innovacion: Deben alentar a las
  personas a crear y sentirse creativas.
  
#### Equipos de desarrollo(Profesionales)

Existen distintas formas de organizar los equipos segun los proyectos
y la misma organizacion:

- _Proyecto_: Un equipo se hace cargo del proyecto de principio a fin.

- _Funcional_: Diferentes equipos toman parte en el desarrollo del
  proyecto, cada uno cumpliendo una funcion especifica.
  
- _Matricial_: Cada proyecto tiene un administrador. Cada persona
  trabaja en uno o mas proyectos bajo la supervision del
  correspondiente administrador.

Para la creacion de un equipo de desarrollo hay que tener varios
factores en cuenta, por ejemplo la rigidez de la fecha de entrega, la
cantidad de gente necesaria, la confiabilidad del sistema, el tamanio
del producto a desarrollar... entre otros.

En cuanto a su estructura interna, existen tres tipos de
organigramas genericos:

- **DD**, descentralizado democratico: Este equipo no tiene un jefe
  definitivo, se asignan coordinadores por tareas a corto plazo que
  van siendo sustituidos a medida que cambian las tareas. La
  comunicacion en este tipo de esquema es horizontal.
  
- **DC**, descentralizado controlado: Este equipo tiene un jefe
  definido que coordina las tareas y jefes secundarios a cargo de las
  subtareas. La solucion de problemas sigue siendo una actividad de
  grupo, pero la implementacion de soluciones se da por subgrupos
  designados por el jefe de equipo. Hay comunicacion horizontal entre
  miembros de equipo y subgrupos, asi como tambien comunicacion
  vertical siguiendo la linea de jerarquia.
  
- **CC**, centralizado controlado: El jefe de equipo se encarga de la
  resolucion de problemas de alto nivel y coordina a los integrantes
  de equipo. La comunicacion se da entre jefe y miembros de equipo, es
  decir que es vertical.

Se busca como ideal que el grupo de desarrollo de un proyecto sea
pequenio y cohesivo, eso trae como beneficio que fomenta el
aprendizaje de uno con otros, es mas facil generar consensos, el
proyecto pasa a ser "propiedad" del grupo. Entre las desventajas
podemos ver que es dificil la adopcion de una autoridad por fuera del
grupo y facilita la toma de decisiones sin explorar alternativas.

    
# Riesgos

> Un riesgo es un evento no deseado que tiene consecuencias negativas.

Es tarea de el gerente determinar si pueden presentarse eventos no
deseados y evitarlos, o si son inevitables minimizar su impacto.

## Gestion de riesgos

La gestion de riesgos en el desarrollo de software concierne a que
sucedera en el futuro con el producto que se desarrolla, es decir que
puede hacer fracasar el desarrollo. Como afectan al exito global y los
plazos, los cambios en la tecnologia, los requisitos del cliente. Que
metodos y herramientas utilizar, cuanta importancia darle a la
calidad... etc.

Existen dos estrategias para abordar los riesgos:

- _Reactivas_: Reaccionar ante el problema y gestionar la crisis.

- _Proactivas_: tener estrategias de tratamiento.

Segun pfleeger podemos abordar el riesgo mediante su identificacion y
para cada riesgo identificado establecer estrategias de respuesta.

Segun Bohem, los items de mas alto riesgo son:

- Deficiencias de personal

- Cronogramas y presupuestos no realistas.

- Desarrollo de funciones incorrectas.

- Desarrollo de interfaces incorrectas.

- Corriente incesante de cambios en los requerimientos.

- Deficiencias en las tareas externas.

Mas alla de las estrategias y las clasificaciones, podemos decir que
los riesgos tienen dos caracteristicas comunes:

- _Incertidumbre_: No hay seguridad de que ocurran 100%.

- _Perdida_: Si sucediese se generaria una perdida.

Entonces el primer paso para la gestion de riesgos es cuantificar
estas caracteristicas, es decir de alguna manera debemos poder dar un
grado de incertridumbre y en cuanta perdida generaria de ocurrir.

En el desarrollo de software podemos clasificar los riesgos como
sigue:

- _Riesgo de proyecto_: Amenazan la planificacion, ya sea el
  presupuesto, los plazos, el personal...
  
- _Riesgo de Producto_: Amenaza al producto en si en su calidad o
  fiabilidad.
  
- _Riesgo de Negocio_: Amenazan a la organizacion que desarrolla el
  software o bien la que lo suministra.
  
Dentro de cada categoria podemos clasificar a los riesgos en forma mas
generica, por ejemplo:

- _Riesgos Genericos_: Son una amenaza potencial en todos los
  proyectos.
  
- _Riesgos Especificos_: Solo los pueden identificar quienes tienen
  una vision clara del contexto(tecnologia, personal, herramintas..)
  
A la vez otra forma de categorizar los riesgos es:

- _Riesgos Conocidos_: Son los que surgen de una cuidadosa evaluacion
  del proyecto.
  
- _Riesgos Predecibles_: Son los que provienen de experiencias
  anteriores.
  
- _Riesgos Impredecibles_: Pueden ocurrir, pero son extremadamente
  dificile de predecir.
  
## El proceso de gestion de riesgos

El proceso es iterativo que debe documentarse y consta de cuatro
etapas:

1. Identificacion de riesgos

2. Analisis de riesgos        <--
                                |
3. Planeacion de riesgos        |
                                |
4. Supervision de riesgos      _|


### Identificacion de riesgos

Consiste en enumerar todos los riesgos, realizar una lista de
elementos de riesgo para la estimacion del impacto de cada uno. Esta
actividad puede realizarse en una lluvia de ideas o tambien basandose
en experiencias previas.

Teniendo un listado de riesgos, se deben categorizar segun algun
criterio. Puede realizarse como se explico previamente(Proyecto,
producto, Negocio) o bien utilizando otras calificaciones. Una vez que
tenemos clasificados los riesgos, pasamos a subclasificarlos como
(conocidos, predecibles, impredecibles).

Por ultimo para su identificacion podemos apoyarnos en las
caracteristicas de los riesgos:

- Impacto(tiempo, calidad, costo, etc)

- Probabilidad(que probabilidad tiene de ocurrir)

- Control(grado en que se puede cambiar el resultado)

### Analisis de riesgo

Se trata de documentar los riesgos enumerados en la etapa anterior,
esta vez cuantificando su impacto y su probabilidad.

En cuanto a su impacto se realiza una escala en donde se establecen
grados de impacto desde que se cancele el proyecto hasta que haya un
minimo impacto. El impacto se estima segun su naturaleza, alcance y
duracion.

En cuanto a la probabilidad, tambien se hace una escala que va desde
"bastante improbable" hasta "bastante probable" y su estimacion puede
ser individual, o bien grupal por promedio.

Con estas dos cuantificaciones se realiza una tabla en donde se listan
los riesgos en forma desordenada, se categorizan, y se
cuantifican. Una vez hecho esto se ordenan por probabilidad e impacto,
y se traza una linea de corte, que indicara cuales riesgos seran
supervisados activamente y cuales son secundarios. Aunque Bohem
propone que la linea de corte sea en 10 riesgos, la realidad es que
dependera del proyecto, la cantidad de gente, la cantidad de
riesgos... en fin, el contexto y naturaleza del proyecto.

### Planeacion de riesgo

Habiendo realizado el analisis y teniendo la linea de corte
establecida, es decir, que riesgos vamos a supervisar activamente;
vamos a planificar la estrategia de tratamiento del riesgo.

La estrategia puede ser de tres tipos:

- _Evitar el riesgo_: Se disenia el sistema de forma que no pueda
  suceder el evento.
  
- _Minimizar el riesgo_: Se trata de reducir la probabilidad de que el
  riesgo suceda.
  
- _Plan de contingencia_: Se trata de estar preparado para lo peor. Se
  acepta la aparicion del riesgo y se trata para minimizar las
  consecuencias.
  
### Supervision de riesgo

Una vez que se realizaron todas las tareas previas y se genero la
documentacion, no se debe mantener una actitud estatica. Por el
contrario, se deben seguir monitoreando los riesgos para identificar
posibles aumentos de probabilidad e impacto; e incluso la aparicion de
nuevos riesgos.

Con cada iteracion se produce una retroalimentacion donde los riesgos
estan gestionados en todo momento.


# Disenio de Interfaz de Usuario

> Es la categoria de disenio que se encarga de crear un medio de
> comunicacion entre el hombre y la maquina.

Es importante hacer hincapie en el disenio de la interfaz de usuario
porque en parte el exito del sistema depende de este aspecto. Un mal
disenio de la interfaz puede provocar que el usuario cometa errores e
incluso se niegue a utilizar el sistema.

Basicamente se trata de integrar todos los aspectos del sistema como
los medios(hipertexto, sonido, presentaciones), el hardware(mouse,
teclado, pantalla, ...), dispositivos(telefonos, tablets, ...) para
hacer que la relacion con el usuario final sea optima, que no sea el
usuario quien debe adaptarse al sistema sino al reves.

En el desarrollo de software entran en juego cuatro modelos:

- El _modelo de usuario_ establecido por el encargado del software. Es
  el modelo que se hace teniendo en cuenta el publico al que va
  destinado el software, es decir que se desarrolla analizando quien
  va a utilizarlo.
  
- El _modelo de disenio_ establecido por el ingeniero de
  software. Es el modelo que establece el ingeniero de software al
  pensar en el usuario final. Frecuentemente, cuando no hay un modelo
  mental bien definido y especifico se suele utilizar este modelo, lo
  que es un error, porque el disenio deja de estar centrado en el
  usuario.

- El _modelo mental_ establecido por el usuario final. Es la
  percepcion que tiene el usuario final sobre el sistema. Vale aclarar
  que es algo que es muy subjetivo, es decir, un usuario puede tener
  una comprension del sistema que hace tener un modelo mental muy
  exacto y puede haber otro para quien el modelo mental es muy vago.

- El _modelo de implementacion_ establecido por los
  implementadores. Es la combinacion de la manifestacion externa del
  sistema junto con toda la documentacion que explique la sintaxis y
  semantica de la misma. Cuando este modelo y el modelo mental
  coinciden, el resultado es un software que le resulta muy comodo al
  usuario; y para que esto suceda el modelo de disenio debe contener
  informacion de ambos modelos.
  
## Proceso de Disenio

El proceso es iterativo en forma de espiral e invlucra cuatro etapas:

- _Analisis y Modelado de Interfaz_: Se trata de identificar todos los
  objetos y acciones de la interfaz.
  
- _Disenio de la interfaz_: Se trata de definir eventos que el usuario
  poda realizar para cambiar el estado de la interfaz.
  
- _Construccion de la interfaz_: Se trata de ilustrar cada estado tal
  cual lo veria el usuario final.
  
- _Validacion de la interfaz_: Se trata de ver como evalua el usuario
  el estado de la interfa a traves de la indormacion que esta brinda.
  
### UdX/UX

>El disenio de experiencia de usuario es un conjunto de metodos
>aplicados al proceso de disenio que buscan satisfacer las necesidades
>del cliente y proporciona una buena experiencia a los usuarios
>destinatarios.

Su funcion es la de hacer converger el **modelo mental** con el
**modelo de disenio** para que los usuarios tengan una buena
experiencia.

Basicamente se consideran distintas fuentes de informacion, como
pueden ser encuestas, informacion de ventas, mercadotecnia.. etc. Se
crea un perfil de usuario teniendo en cuenta edad, clase social,
genero, experiencias, idioma, nivel de estudios, ... por ejemplo. Se
hace un relevamiento de la tarea y el contexto de trabajo. Basicamente
en que contexto sera utilizado el sistema y como sera utilizado.


## Estilo de Interfaces

- **Interfaz de comandos:** Es la mas elemental. Solo se interactua con
  texto. Tiene como caracteristica que es muy flexible y poderosa;
  tiene un pobre manejo de errores y suele ser dificil de aprender
  para el usuario.
  
  - _Interfaz de comandos graficas_: presenta una interfaz grafica
    para ingresar comandos. 
  
  - _Interfaz de comandos tipo pregunta-respuesta_: El flujo de
    trabajo es mediante comandos y respuestas que lo encausan. 
  
  - _Interfaz de menu simple_: Se presenta un menu con opciones para
    el usuario y se provee a cada item de una letra para que el
    usuario elija. Tiene como caracteristica que limita los errores
    que puede cometer el usuario y es lenta en caso de usuarios
    experimentados.
    
  
- **Interfaz grafica de usuarios(GUI):** Se caracterizan por la
  utilizacion de todo tipo de recursos para comunicarse con el
  usuario. Tiene como caracteristica que son relativamente faciles de
  utilizar y aprender, los usuarios tienen multiples
  pantallas(ventanas) para acceder a cualquier parte del sistema.
  
  - _Ventanas_
  
  - _Iconos y menus_
  
  - _Interfaces de manipulacion directa_: Pantallas tactiles. Llevan
    hardware especifico.
    
- **Interfaz de reconocimiento de voz:** La comunicacion con el
  usuario es por medio de la voz
  

## Aspectos del disenio

Exiten tres reglas denominadas "de oro" al diseniar una interfaz:

- _Dar control a usuario_: es decir que el sistema reacciones a las
necesidades del usuario y lo ayude a realizar sus tareas.

- _Reducir la carga de memoria_: Se trata de definir valores por
defecto que sean significativos, que los accesos directos sean
intuitivos y que el formato visual de la interfgaz debe ser una
metafora de la realidad.

- _Lograr una interfaz consistente_: permitir que el usuario incluya
  la tarea actual dentro de un contexto que tenga algun
  significado. Mantener la consistencia de la familia de
  aplicaciones y mantener tambien los modelos que son practicos al
  usuario.
  
Tambien podriamos agregar factores humanos, como por ejemplo la
dicersidad de usuarios(usuarios casuales y experimentados), asi
tambien como memoria, diferentes percepciones, etc.

### Principios de Nielsen

1. **Dialogo simple y natural.** Referente a como el usuario se debe
   relacionar con el sistema. Basicamente se trata de que la escrtura
   sea correcta, que no haya excesivas mayusculas ni
   abreviaturas. Que la distribucion de informacion sea adecuada, que
   no se mezcle la informacion relevante con la irrelevante.
   
2. **Lenguaje de usuario.** Utilizar el mismo lenguaje que el usuario,
   tratar de evitar el lenguaje tecnico y extranjero; y utilizar una
   cantidad de informacion adecuada(ni mucho ni poco)
   
3. **Minimizar el uso de la memoria.** Se trata de que el usuario no
   tenga que memorizar la forma de interactuar con el sistema. Se
   trata de brindar indormacion del contexto, la sesion actual y l
   navegacion. Visualizacion de rangos de entradas adminsibles,
   formatos y ejemplos.
   
4. **Consistencia.** Que no existan ambiguedades en el aspecto visual
   ni tecnologico en el dialogo o en el comportamiento del sistema. La
   consistencia es un punto clabe para ofrecer confiabilidad y
   seguridad al sistema.
   
5. **Feedback.** Es una respuesta grafica o textual en pantalla,
   frente una accion del usuario. El sistema debe mantener al usuario
   informado de lo que esta sucediendo. Brindar informacion de los
   estados de los procesos. Utilizacion de mensajes de aclaracion,
   validaciones, confirmacion y cierre. Realizar validacion de los
   datos ingresados por los usuarios.
   
6. **Salidas evidentes.** Que el usuario tenga a su alcance de forma
   identificable y accesible una opcion de salida. Brindarlas a cada
   pantalla. Salidas para cada contexto. Salidas para cada accion,
   tarea o transaccion. Brindar salidas en cada estado. Visualizacion
   de opciones de cancelar, suspender, deshacer y modificacion.
   
7. **Mensajes de error.** Feedback del sistema ante la presencia de un
   error. De que forma se ayuda al sistema a salir de la situacion en
   la que se encuentra. No deben existir mensajes de error
   intimidatorios. Manejar de forma adecuada la aparicion de estos
   mensajes. Brindar informacion del error, explicar el error y dar
   alternativas a seguir.
   
8. **Prevencion de errores.** Evitar que el usuario llegue a una
   instancia de error. Brindar rangos de entradas posibles para que el
   usuario seleccione y no tipee. Flexibilidad en las entradas de los
   usuarios.
   
9. **Atajos.** La interfaz deberia proveer alternativas de manejo para
   que les resulte comodo y amigable, tanto para usuarios novatos como
   para usuarios experimentados.
   
10. **Ayudas.** Componentes de asistencia para el usuario. Un mal
    disenio de las ayudas puede llegar a entorpecer y dificultar la
    usabilidad. Deben existir las ayudas y se deben brindar distintos
    tipos: Contextuales, generales, especificas, en linea...

### Presentacion de la informacion

Se debe presentar la informacion que el usuario necesita y de la forma
que le resulte mas facil procesar e identificar; para ello se deben
conocer a los usuarios y como utilizaran el sistema.

La utilizacion de los colores para este fin es importante, para ello
debe limitarse la cantidad de colores que se utilizan, deben ser
utilizados de forma consistente y combinarlos cuidadosamente; aunque
tampoco deben ser el unico metodo para mostrar cambios en el estado
del sistema(10% de los usuarios no distingue colores).

### Soporte al usuario
Mensajes del sistema por acciones de usuario. Soporte en
linea. Documentacion del sistema.


## Interfaces inteligentes

Tienen la capacidad de captar las acciones que el usuario repite con
frecuencia para luego brindar la posibilidad de completarlas
automaticamente.

Dentro de este tipo encontramos:

- _Adaptativas_:Brindan diferentes modos de interaccion de acuerdo al
  tipo de usuario en cuestion. Son sensibles a los perfiles de usuario
  y a sus estioos de interaccion.
  
- _Evolutivas_: Tienen la propiedad de cambiar y evolucionar segun
  pasa el tiempo y el grado de perfeccionamiento que va adquiriendo
  con el sistema.
  
- _Interfaces Accesibles_: Son las interfaces que respetan las normas
  de disenio universal para que puedan seer accedidas por cualquier
  usuario sin importar su condicion fisica.
  

## Usabilidad

>La usabilidad es una medida de cuan bien un sistema de computo [...]
>facilita el aprendizaje, ayuda a quienes lo emplean a recordar lo
>aprendido, reduce la probabilidad de cometer errores, permite ser
>eficientes y los deja satisfechos con el sistema.

La forma de determinar si existe usabilidad en un sistema es evaluarla
y probarla. Para ello se les pide a los usuarios que interactuen con
el sistema y contesten una serie de pregunstas. La idea es que
respondan que si a la mayoria de preguntas.

# Metricas

Las metricas son una parte importante de la gestion de
proyectos. Sirven para entender,controlar, mejorar y evaluar.

- _Entender_ que sucede durante el proceso de desarrollo y
  mantenimiento.
  
- _Controlar_ que es lo que ocurre en nuestros proyectos.

- _Mejorar_ nuestros procesos y nuestros productos.

- _Evaluar_ la calidad.

>Una metrica es una medida cuantitativa del grado en que un sistema,
>componente o proceso posee un atributo dado.

El ingeniero de software recopila medidas y genera metricas para
obtener indicadores.

>Un indicador es una combinacion de metricas. Proporciona una vision
>profunda que permite al gestor de proyecto o a los ingenieros de
>software ajustar el producto, el proceso o proyecto para que las
>cosas salgan mejor.

Por todo lo dicho previamente podemos afirmar que las metricas sirven
para la toma de decisiones y son un medio para asegurar la calidad en
los productos/procesos/proyectos.

Se pueden clasificar en dos tipos:

- _Metricas de control_: que sirven para apoyar la gestion del proceso
  de software.
  
- _Metricas de prediccion_: conocidas tambien como _metricas de
  producto_, sirven para predecir las caracteristicas del software.
  
## Metricas de producto

Las metricas de producto se dividen en dos categorias. 

- _Metricas dinamicas_: Son las que deben recopilarse con el programa
en ejecucion(ej: cantidad de logs) y ayudan a valorar la eficiencia y
fiabilidad de un sisema

- _metricas estaticas_: Son las que se recopilan a partir de una
representacion del programa y ayudan a valorar
la complejidad, comprensibilidad y mantenibilidad de un sistema de
software. Ejemplos:
    - Fan-in/fan-out
    - Longitud del codigo
    - Complejidad idiomatica
    - Longitud de los identificadores
    - Indice de fog
    
Las metricas solo son utiles si estan caracterizadas de manera
efectiva y se validan para probar su valor. Los principios que deben
utilizarse para esto son:

* Las metricas deben tener **propiedades matematicas deseables**(rango
  significativo).

* Cuando una metrica representa una **caracteristica de software** que
  aumenta cuando se presentan rasgos positivos y por el contrario
  disminuye con los rasgos negativos; su valor debe aumentar y
  disminuir en el mismo sentido.
  
* Cada metrica debe **validarse empiricamente** en una amplia variedad
  de contextos antes de publicarse o aplicarse a la toma de
  decisiones.
  

## Metricas de control

Un ejemplo de metrica de control es el PF(Punto funcion). Dicha
metrica trata de medir un modelo para predecir su tamanio, es decir se
trata de ver cuanta funcionalidad se establece.

En base a eso, se establece la complejidad del sistema y se obtiene un
valor, desde donde podemos predecir el costo, la productividad y la
calidad.


## GQM(Goal Question Metric)

>Es un metodo orientado a lograr una metrica que mida cierto
>objetivo. El mismo nos permite mejorar la calidad de nuestro
>proyecto.

Define tres niveles:

- _Nivel Conceptual_: Se define el objectivo(Goal).

- _Nivel Operativo_: Se definen un conjunto de preguntas con el fin de
  evaluar el cumplimiento de nuestro objectivo.
  
- _Nivel Cuantitativo_: Se asocia a cada pregunta un conjunto de
  metricas de forma de responder a cada una de forma cuantitativa.
  
Ejemplo: **Asignacion de responsabilidades**.

# Estimaciones

Las estimaciones permiten dar un valor aproximado, como tales no son
una ciencia exacta.

Se requiere acceso a la historia previa, a experiencia y decision para
transmformar informacion que es cualitativa en cuantitativa(estimacion
en si). Por esto mismo es que el riesgo de una estimacion disminuye
con la disponibilidad de la historia

Las estimaciones que suelen realizarse en un proyecto son de costos,
de tiempos y de recursos.

## Costos

Se trata de estimar cuanto va a costar construir el software y se
divide en tres items:

- _Costo de esfuerzo_: Cuantos desarrolladores/mes se necesitaran y
  cuanto se les pagara.
  
- _Costo de hardware y software_: incluido no solo lo que se necesita
  sino su mantenimiento.
  
- _Costos de viaje_.

Dentro de los costos esta la fijacion de precios. Inicialmente se
podria decir que hay que tener en cuenta solo el costo de desarrollo;
pero en realidad tambien hay que tener en cuenta otros factores como
la organizacion, el riesgo en la estimacion del costo, el mercado...

## Recursos

Para la estimacion de recursos tambien se tienen en cuenta tres
categorias:

- _Recursos Humanos_.

- _Recursos de software Reutilizables_.

- _Recursos de hardware y herramientas de software_.

Donde cada recurso requiere descripcion, informe de disponibilidad,
fecha, tiempo que se lo necesita.
## Tecnicas de estimacion

Las hay varias, entre las mas notables:

- _Juicio Experto_: Se consultan varios expertos, cada uno de ellos
  estima. Se comparan y discuten.
  
- _Tecnica Delphi_: Se trata de la seleccion de un grupo de expertos
  que realizan una estimacion en sucesivas rondas y en forma anonima,
  con el objetivo de conseguir consenso, pero con maxima autonomia por
  parte de los participantes.
  
- _Division de trabajo_: Basicamente una jerarquia donde la parte mas
  baja estima lo concerniente a su ambito, y las partes superiores
  estiman en base a las estimaciones inferiores.
  

## Modelos Empiricos de Estimacion (COCOMO)

**COCOMO** hace referencia a **CO**nstructive **CO**st **MO**del. Es
una forma empirica de estimar costos u esfuerzos requeridos para un
proyecto. Se desarrollaron distintas formulas en base al analisis de
varios grandes proyectos. Estas formulas vinculan el tamanio del
sistema y del producto, factores del proyecto y equipo, con el
esfuerzo necesario para desarrollar el sistema.

A grandes rasgos existen dos modelos:

- _COCOMO81_: Se basa principalmente en lineas de codigo y reconoce
  tres tipos de sistemas(organicos, semiacoplados y empotrados) que
  funcionan como factores de calculo.
  Propone distintos niveles de estimacion, a medida que se va bajando
  de nivel esta se vuelve mas fina.
  
- _COCOMOII_: Es una actualizacion de COCOMO81. Reconoce que estimar
  las KLDC tempranamente es dificil. Considera distintas etapas en el
  desarrollo como por ejemplo contruccion de prototipos y desarrollo
  en espiral. Al igual que COCOMO81 plantea varios niveles en donde a
  medida que se van aplicando se obtiene una estimacion mas fina.


# Disenio de software

Es el proceso cretivo de transformacion de un problema en una
solucion. Es el nucleo tecnico de la Ingenieria de software.

Se divide en cuatro areas:

## Disenio de datos

Es la transformacion de modelos del dominio en estructuras de datos,
relaciones, etc.

## Disenio de Arquitectura

Define la relacion entre los elementos estructurales mas importantes
del software, como por ejemplo patrones de disenio, estilos
arquitectonicos, etc; para lograr los requisitos del sistema.

## Disenio a nivel componentes

Traduce los elementos estructurales de la arquitectura en una
descripcion procedimental de los componentes del software. Parte de la
base de DTEs, clases, modelos de flujos... etc.

## Disenio de interfaces

Describe la forma de comunicacion dentro del sistema, con otros
sistemas y con las personas.

## Criterios tecnicos para la evaluacion de un buen disenio

En la etapa de disenio es cuando se debe fomentar la calidad, ya que
proporciona las representaciones del software susceptibles de ser
evaluadas respecto de ella; y si partimos de un mal disenio, o incluso
inexistente, se llegara a un sistema inestable, donde se dificulta la
inclusion de cambios por mas minimos que sean y sera dificil de
probar.

Un buen disenio:

* Debera implementar todos los requisitos explicitos del modelo de
  analisis y debera ajustarse a los requisitos implicitos del cliente.
  
* Debera ser una guia comprensible para aquellos que generan lineas de
  codigo como para quienes lo comprobaran, y subsecuentemente lo
  mantendran.
  
* Debera proporcionar una imagen completa del software, enfrentandose
  al dominio del comportamiento funcional y de datos con una
  perspectiva de implementacion.
  
Teniendo estos lineamientos podemos enumerar los **criterios tecnicos
para un buen disenio.** Los cuales son:

1. Un disenio debera presentar una estructura arquitectonica que: se
   haya creado con patrones de disenio reconocibles, que este formado
   por componentes que exhiban caracteristicas de buen disenio y se
   implemente de forma evolutiva.

2. Un disenio debera ser modular. Debera dividirse logicamente en
   elementos que realicen funciones y subfunciones especificas.
   
3. Un disenio debera contener distintas representaciones de datos,
   arquitectura, interfaces y componentes(modulos).
   
4. Un disenio debera conducir a estructuras de datos adecuadas para
   los objetos que se van a implementar y respetar patrones de datos
   reconocibles.
   
5. Un disenio debera conducir a componentes que presenten
   caracteristicas funcionales independientes.
   
6. Un disenio debera conducir a interfaces que reduzcan la complejidad
   de las conexiones entre los modulos y con el entorno externo.
   
7. Un disenio debera derivarse mediante un metodo repetitivo y
   controlado por la informacion obtenida durante el analisis de los
   requisitos del software.
   
8. Un disenio debe representarse por medio de una notacion que
   comunique de manera eficaz su funcionamiento.
   

## Conceptos de disenio
### Abstraccion

La nocion de abstraccion permite concentrarse en un problema a un
nivel de generalizacion sin tener en cuenta los detalles irrelevantes
de bajo nivel. A medida que vamos implementando la solucion llegamos al
menor nivel de abstraccion (codifo fuente)

Se puede hablar de **abstraccion** a dos niveles:
procedimental(procedimientos, funciones, modulos... etc) y abstraccion
de datos.

### Arquitectura

Es la estructura general del software y la forma en que la estructura
proporciona integridad conceptual para un sistema.

### Patrones

Es una estructura de disenio que resuelve un problema particular
dentro de un contexto especifico.

La finalidad de un patron de disenio es proporcionar una descripcion
que permita definir al diseniador si:

- Es aplicable al trabajo actual.

- Se puede reutilizar

- Puede servir como guia para implementar un nuevo patron similar pero
  diferente en cuanto a estructura o funcionalidad.
  
### Modularidad

El software se divide en componentes nombrados y abordados por
separado, llamados modulos, que se integran para satisfacer los
requisitos del problema.

Se puede hablar de disenios **monoliticos**(un unico modulo) o
**excesivamente modularizados**(a nivel instruccion).

### Ocultamiento de informacion

La informacion de un modulo no debe ser accesible para otro modulo que
no la necesite. Se consigue modularidad efectiva cuando los modulos
son independientes y se comunican entre si compartiendo solo la
informacion que necesitan para funcionar.

### Independencia funcional

Es la combinacion entre **abstraccion** + **modularidad** +
**Ocultamiento de la informacion**. Es deseable que cada modulo trate
una subfuncion de requisitos y tenga una interfaz sencilla para que
sea mas facil desarrollar, probar y reusar.

Se trata de maximizar la cohesion y minizar el
acoplamiento. Entendiendo **cohesion** como:

>La medida medida de fuerza o relacion que existe entre las sentencias
>o un grupo de sentencias de un mismo modulo.

Se dice que un modulo es altamente cohesivo cuando realiza una sola
tarea dentro del procedimiento y necesita poca interaccion con el
resto de los procedimientos.

Entendemos acoplamiento como:

>La medida de interconexion entre los modulos.

Una interconectividad sencilla entre modulos da como resultado una
conectividad mas facil.


### Refinamiento

El refinamiento es un proceso de elaboracion iterativo. Se comienza
con una descripcion de la informacion de alto nivel, sobre una
funcionalidad puntual, sin conocer las caracteristicas de disenio. Se
va trabajando sobre la funcionalidad original pasando por sucesivas
iteraciones hasta alcanzar el nivel de detalle deseado(o necesario).

Es un concepto complementario de la abstraccion. La abstraccion
permite especificar datos y procedimientos sin considerar los
detalles, mientras que el refinamiento permite revelar los detalles a
medida que avanzamos en el disenio.

### Refactoring

Es una tecnica de reorganizacion(sugerida en metodologias agiles) que
simplifica el disenio de un componente sin cambiar su funcion o
comportamiento.

Ej. Una primera iteracion del disenio podria producir un componente
con poca cohesion. Entonces el diseniador podria decidir implementar
nuevamente el modulo en modulos distintos en la proxima iteracion.

# Arquitecturas

>La arquitectura define la relacion entre los elementos estructurales
>para lograr los requisitos del sistema.

Los sistemas se dividen en subsistemas que proporcionan un conjunto de
subsistemas; el **disenio arquitectonico** es el proceso de
identificar estos subsistemas y establecer un marco de control para la
comunicacion entre ellos.

Como se dijo previamente la arquitectura incide en lograr los
requisitos del sistema, mas especificamente, la arquitectura afecta
directamente a los requerimientos no funcionales. Los mas criticos
son:

- _Rendimiento_: Se deben agrupar las operaciones criticas en un grupo
  reducido de sub-sistemas. (grano grueso y baja comunicacion)
  
- _Seguridad_: Se debe utilizar una arquitectura en capas, protegiendo
  los recursos mas criticos en las capas internas.
  
- _Proteccion_: La arquitectura se debera diseniar para que las
  operaciones relacionadas con la proteccion se localicen en un unico
  sub-sistema.
  
- _Mantenibilidad_: La arquitectura debera diseniarse con componentes
  autocontenidos de grano grueso que sean facilmente modificables.
  
- _Disponibilidad_: La arquitectura debera diseniarse con componentes
  redundantes que sean facilmente reemplazables.
  
## Disenio Arquitectonico

### Organizacion del sistema

Representa la estrategia basica utilizada para estructurar el
sistema. Dado que los subsistemas tienen informacion propia y muchas
veces deben compartir informacion con otros sistemas, estas
estrategias se centran en como se realiza este intercambio de
datos. Entre las distintas organizaciones podemos mencionar:

#### Repositorio

Consta de una base de datos central sobre la cual se organizan los
distintos subsistemas. Los datos se generan en un subsistema y son
utilizados por otro subsistema.

Las **ventajas** de esta organizacion es que como todos los subsistemas
trabajan con una unica bbdd central, se elimina el transporte de datos
entre modulos lo que favorece a aquellos sistemas que necesitan
manejar muchos datos. Tambien las nuevas herramientas se integran mas
facilmente ya que deben interactuar con una base de datos unicamente y
las actividades de manejo de datos(backup, proteccion, control de
acceso) se realizan en un unico punto.

Las **desventajas** de este tipo de organizacion son que muchas veces
un subsistema necesita mayor o menor proteccion de los datos, al estar
centralizado se debe aplicar a todos los datos de todos los
modulos. Los subsistemas deben mantenerse acorde a los modelo de datos
del repositorio, lo que puede afectar el rendimiento; y finalmente es
dificil distribuir el repositorio central en varias maquinas sin caer
en problemas de inconsistencia y redundancia.


#### Cliente-Servidor

Esta organizacion consta de tres elementos, **servidores** que ofrecen
datos a traves de servicios a **clientes** con una **red intermedia** que
permite su comunicacion.

Basicamente tenemos de un lado servidores con los datos corriendo
servicios, y clientes del otro que conocen donde estan los servidores
y sus servicios. Los clientes piden datos a traves de la red.

Esta arquitectura permite que los servidores respondan a los clientes
sin necesidad de saber quienes son.

#### Arquitectura en Capas

Consiste en estructurar el sistema en capas que ofrezcan servicios a
sus capas adyacentes.

Tiene la **ventaja** de que soporta el desarrollo incremental y es facil
intercambiar una capa siempre y cuando mantenga la interfaz, o bien
agregando capas que sirvan de adaptador. Tambien permiten crear
sistemas multiplataforma donde los servicios que dependen del sistema
operativo sean las mas bajas.

Como **desventaja** podemos mencionar que generalmente son dificiles
de estructurar y las capas interiores implementan servicios que son
requeridos por todos los niveles, entonces los servicios requeridos
por los usuarios en niveles superiores deben atravesar varias capas
adyacentes. Ademas si existen muchas capas es muy posible que un mismo
servicio sea interpretado varias veces en las distintas capas
intermedias.

### Descomposicion modular

Antes de hablar de la descomposicion modular debemos dejar en claro la
diferencia entre subsistema y modulo. El primero se considera un
sistema en si, que es independiente de otros sistemas en el sentido de
que no depende de servicios proporcionados por otros. Un modulo es
parte de un sistema y no es independiente, implementa servicios que
utilizaran otros modulos y utilizara servicios implementados en otros
modulos.

Dicho esto, podemos decir que la descomposicion modular es un grado
mas de subdivision, que se realiza dentro de los sistemas en donde se
pueden aplicar las mismas estrategias que en la organizacion del
sistema, pero al ser mas fino, tambien pueden aplicarse otros estilos
alternativos. Podemos mencionar dos:

- _Descomposicion orientada a flujo de funciones_: En este tipo de
  modelo los datos fuyen de una funcion a otra y se transforman en
  cada paso llegando a la salida. El procesamiento de cada funcion
  puede ser secuencial o paralelo.
  
- _Descomposicion orientada a objetos_: En este modelo nos encontramos
  con objetos debilmente acoplados y con interfaces bien definidas.
### Modelos de control

Se refiere al control de que los servicios se entreguen en el lugar y
momento preciso.

Existen dos modelos de control:

#### Control centralizado

El control centralizado se caracteriza por definir un subsistema en
forma de controlador que se encarga exclusivamente de la ejecucion de
otros subsistemas.

La ejecucion puede ser secuencial o paralela:

- _Modelo de llamada y retorno_: Aplicable a la ejecucion
  secuencial. Se trata de llamadas a subrutinas en forma descendente.
  
- _Modelo de gestor_: Aplicable a la ejecucion paralela. Gestiona el
  inicio y parada de procesos en coordinacion con los demas procesos.


#### Control manejado por eventos

Se rigen por eventos generados externamente al proceso. Los eventos
pueden ser cualquier senial. Los dividimos en dos:

- _Modelo de transmision(broadcast)_: El evento es transmitido a todos
  los subsistemas y el subsistema que sepa como responder a el lo
  manejara.
  
- _Modelo dirigido por interrupciones_: Utilizado en sistemas de
  tiempo real donde los eventos externos son captados por un manejador
  de interrupciones que lo envia a algun componente para que lo
  maneje.

## Sistemas distribuidos

>Es un sistema en el que el procesamiento de informacion se distribuye
>sobre varias computadoras.

Los sistemas distribuidos tienen la caracteristica de:

- _Compartir recursos_

- _Apertura_: Son desarrollados sobre protocolos estandares para
  simplificar la combinacion de recursos.
  
- _Concurrencia_: Al ejecutarse en varias computadoras permite que se
  ejecuten paralelamente las tareas.
  
- _Escalabilidad_: Simplemente agregando recursos se puede hacer
  frente a una mayor demanda.
  
- _Tolerancia a fallos_: La disponibilidad de varias computadoras y el
  potencial para reproducir informacion hace que los sistemas
  distribuidos sean mas tolerantes a fallos de hardware y de software.
  
Entre sus desventajas podemos mencionar:

- _Complejidad_: Son mas complejos que los centralizados porque ademas
  del sistema hay que tener en cuenta la comunicacion y sincronizacion
  de equipos.
  
- _Impredecibilidad_: El tiempo de respuesta no depende solo del
  sistema sino que tambien depende de la congestion de la red de
  intercomunicacion.
  
- _Seguridad_: Como los equipos se intercomunican sobre un medio
  comun, este trafico es susceptible de que alguien lo intercepte.
  
- _Manejabilidad_: Las computadoras pueden ser de diferentes tipos y
  tener distintos SO, lo que agrega una capa mas de complejidad al
  sistema.
  
Dentro de las arquitecturas distribuidas podemos hablar de:

### Multiprocesador

Se trata de que el sistema esta compuesto por distintos procesos
independientes que pueden o no ejecutarse en procesadores
diferentes.

La asignacion de cada proceso a cada procesador esta dada por un
dispatcher. Es una arquitectura muy utilizada en **sistemas de tiempo
real** donde se esta tomando informacion del ambiente, se toman
decisiones y se modifica el contexto constantemente.

### Cliente-Servidor

La aplicacion se modela como un conjunto de procesos diferentes
agrupados en clientes y servidores donde los servidores proporcionan
servicios y los clientes los consumen. Este tipo de organizacion
permite a los servidores no tener que conocer los clientes e incluso a
los clientes no tener que conocerse entre si. Los clientes solo deben
conocer los servidores y los servicios que brindan.

Este modelo lo dividimos segun su cantidad de niveles:

- _Dos niveles_: Uno cliente y otro servidor, en donde el cliente
  puede ser liviano o pesado. El primero solo se encarga de la
  presentacion de informacion delegando en el servidor toda la logica
  y manejo de los datos. El segundo contiene logica no solo de
  presentacion sino que tiene logica de la aplicaccion, el servidor
  solo se encarga del manejo de los datos.
  
- _Multinivel_: Hay vaios niveles. En uno el cliente(que es liviano,
  solo presenta informacion), en otro el servidor que contiene logica
  de la aplicacion, y en otro los servicios de la aplicacion que
  utiliza el servidor(como base de datos).

### Componentes distribuidos

Consta de dieniar al sistema como varios objetos o componentes que
mediante una interfaz conocida brindan un conjunto de servicios. Estos
servicios seran consumidos por otros objetos.

A diferencia de la arquitectura de cliente servidor, no hay clara
distincion entre clientes y servidores.

### Computacion distribuida interorganizacional
### Peer to Peer(P2P)

Se disenian para aprovechar la ventaja computacional y de
almacenamiento de una red de computadoras. Se desarrollan de tal modo
que el procesamiento se pueda realizar en cualquier nodo de la
red. Puede ser descentralizada o semi-descentralizada; en la primera
un nodo envia un paquete a sus vecinos hasta que este encuentra su
destino y en la segunda existen nodos servidores que ayudan a conectar
a nodos clientes.

### Arquitectura orientada a servicios

Los servicios son una abstraccion de los recursos y son independientes
de la applicacion que los utiliza. De esta forma un servicio puede ser
utilizada por distintas organizaciones y las aplicaciones pueden
contruirse enlazando servicios.

Normalmente existe un suministrador de servicios que publica sus
servicios en un registro de servicios, que es consultado por un
solicitante de servicios.

El solicitante del servicio, al tener los datos del servicio y los
parametros para invocarlo, lo hace y comienza a utilizar el servicio.

La comunicacion entre servicios se da respetando ciertos estandares
como SOAP, WSDL o UDDI.



# Pruebas
## Enfoque estrategico de pruebas

Una estrategia de pruebas del software es una guia que describe los
pasos a seguir, cuando se planean y llevan a cabo, cuanto esfuerzo,
tiempo y recurso se requeriran.

El proceso de prueba del software se divide en etapas:

- _Planificacion_ de los casos de prueba.

- _Disenio_ de los casos de prueba.

- _Ejecucion_ de las pruebas.

- _Recoleccion_ y _evaluacion_ de los casos de prueba.

A la vez tenemos distintos tipos de pruebas, cada uno orientado a
probar distintas partes y funcionalidades del sistema.

## Tipos de pruebas de software
### Pruebas de unidad

Se verifica que el componente funciona correctamente a partir del
ingreso de distinto casos de prueba. 

En este tipo de pruebas se detectan errores como calculos incorrectos,
comparaciones erroneas y flujos de control inapropiados.

Estas pruebas constan de un programa controlador que es el encargado
de proveer las entradas al modulo y mostrar las salidas; y resguardos,
que son reemplazos de otros modulos, como bien sabemos los modulos
pueden depender de otros modulos, entonces se les suministra los
resguardos para que puedan funcionar.

### Pruebas de integracion

Las pruebas de integracion verifican que los modulos trabajan
conjuntamente en forma correcta. Es una tecnica sistematica para
construir la arquitectura de software mientras al mismo tiempo se la
prueba.

Los modulos que se prueban en esta etapa son aquellos que ya han
pasado las pruebas de unidad, es decir que sabemos que funcionan como
se espera.

Detecta errores como que se pierda informacion entre interfaces, que
un modulo tenga efectos secundarios sobre otros, que la combinacion
entre modulos no tengan el efecto esperado...

La integracion puede ser descendente o ascendente.

En el caso de integracion **descendente** los modulos se van
integrando partiendo del programa principal hacia abajo, proveyendo
resguardos y en cada iteracion reemplazando resguardos por modulos. A
la vez esta integracion descendente puede ser **en profundidad** o
**en anchura**.

La integracion **ascendente** consta de probar primero los modulos de
mas bajo nivel e ir integrando en cada iteracion los modulos de mas
alto nivel hasta llegar al programa principal. Tiene la caracteristica
de que no necesita resguardos.. aunque si conductores.

Cada opcion tiene sus ventajas y desventajas; y no hay una criterio
definitorio para elegir uno u otro... lo que se suele realizar son
pruebas **sandwich** que no es mas que aplicar ambos tipos de pruebas.

La **pruebas de regresion** no es mas que volver a realizar las
pruebas de integracion, una vez que se agrega un modulo mas a las
pruebas. Dado que esto puede provocar cambios en las E/S y hay una
nueva logica de control. Pueden realizarse en forma automatica o
manual, y pueden aplicarse de distinta forma; realizando de nuevo
todos los casos de pueba, probar solo aquellas funcionalidades que
pudieron verse afectadas por los cambios y/o pruebas que se centren
solo en los componentes que fueron integrados.

Tambien se pueden aplicar pruebas segun la **criticidad**, en donde se
definen cuales son los modulos criticos. Estos modulos pueden
identificarse porque abordan muchos requerimientos, son proclives al
fallo, tienen alto nivel de control o bien tienen requerimientos de
rendimiento definidos. En cualquier caso, estos modulos son los
primeros que deben testearse y las pruebas de regresion deben hacer
foco en ellos.


### Pruebas del sistema

Estas pruebas estan destinadas a probar que los componentes de un
sistema estan bien integrados y que el sistema en su totalidad
funciona como es esperado.

Se trata de un conjunto de pruebas donde cada una persigue un
objetivo distinto pero en conjunto sirven al objetivo general.

Los tipos de prueba son:

- _Pruebas de recuperacion_: Se prueba la recuperacion de fallas y el
  modo de reanudacion. Generalmente se provocan las fallas a
  proposito.
  
- _Pruebas de seguridad_: Se comprueban mecanismos de proteccion
  integrados.
  
- _Pruebas de resistencia(stress)_: Se disenian para enfrentar a los
  programas a situaciones anormales.
  
- _Pruebas de rendimiento_: Se prueba resistencia en tiempo de
  ejecucion.

### Pruebas de validacion

Se trata de probar que el software cumple con los requerimientos tanto
funcionales como no funcionales. Estas pruebas se realizan luego de
las pruebas de integracion y su resultado puede ser uno de dos:

* Las caracteristicas de funcionamiento y rendimiento estan de acuerdo
  a las especificaciones y son aceptables.
  
* Se encontro una desviacion de las especificaciones y se genera una
  lista de deficiencias.
  
Las **pruebas de aceptacion** son parte de las pruebas de validacion y
las hay de dos tipos: **alfa** y **beta**.

En ambos tipos la prueba la realiza el usuario final sobre el sistema
que se entrega(o libera). La diferencia radica en el momento en que se
realiza, el contexto y el sujeto que realiza la prueba.

La prueba **alfa** es realizada por un cliente en presencia de un
desarrollador quien anota como el usuario utiliza el sistema, los
problemas que encuentra y los errores que surjan. Es decir, el
contexto esta controlado por el desarrollador y se suele realizar
antes de la liberacion del software, el sistema no esta en su version
final.

La prueba **beta** se realiza en ausencia del desarrollador y en los
lugares donde el sistema va a ser utilizado. Debido a esto es que el
contexto no es controlado. Es la ultima etapa de prueba, en donde
puede incluso liberarse el sistema en version beta para que ya
comience a utilizarse.

## Depuracion

>Es el proceso de identificar y corregir errores en sistemas
>informaticos.

**La depuracion no es una prueba**, pero siempre ocurre como
consecuencia de una prueba efectiva. Es decir, se descubre un
error.. la depuracion lo corrige.

El proceso de depuracion tiene un resultado de dos posibles:

1. Se encuentra la causa, se corrige y elimina.

2. No se encuentra la causa, pero la persona que realiza la depuracion
   sospecha la causa. Disenia un caso de error y una prueba que ayude
   a confirmar su sospecha. Y el trabajo vuelve atras a la correccion
   del error a una forma iterativa.
   
Los errores pueden tener multiples origenes y tener muchas
caracteristicas distintivas. Por ello es que la depuracion debe
realizarse siguiendo multiples enfoques tambien. Por ejemplo:

* Diseniar programas de prueba adicionales que repitan la falla
  original y ayuden a descubrir la fuente de falla en el programa.
  
* Rastrear el programa manualmente y simular la ejecucion.

* Usar herramientas interactivas.

* Una vez corregido el error debe reevaluarse el sistema: Volver a
  hacer las inspecciones y repetir las pruebas.
  

## Prueba en entornos especializados

A medida que el software crece y se hace mas complejo, crece tambien
la necesidad de enfoques de pruebas especializados.

- _Pruebas de interfaces graficas_

- _Prueba de arquitectura cliente-servidos_: Se prueba por ejemplo la
  coordinacion del servidor, el manejo de datos, la base de
  datos(integridad, validez, recuperacion), las transacciones,
  comunicaciones de red...
  
- _Prueba de la documentacion y funciones de ayuda_: Se revisan la
  documentacion de usuario o funciones de ayuda en linea.
  
- _Prueba de sistemas de tiempo real_: En este tipo de sistemas ademas
  de los casos de prueba usuales se debe probar el manejo de eventos,
  interrupciones, paralelismo de tareas... entre otros.
